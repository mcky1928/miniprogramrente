// pages/equipment/equipment.js
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    Host: app.globalData.Host,
    boxHeight: 0,
    showZqmodal: false,
    shopId: '',
    user: {},
    page: 1,
    pageSize: 10,
    loadAll: false,
    scrolltoupper: false,
    scrolltolower: false,
    imgUrls: [
      'https://images.unsplash.com/photo-1551334787-21e6bd3ab135?w=640',
      'https://images.unsplash.com/photo-1551214012-84f95e060dee?w=640',
      'https://images.unsplash.com/photo-1551446591-142875a901a1?w=640'
    ],
    indicatorDots: true,
    autoplay: true,
    circular: true,
    interval: 5000,
    duration: 1000,
    equipmentList: [],
    selectedOrder: {},
    sortArr: [
      { name: '类别', property: 'uuid', checked: true, sort: false },
      { name: '型号', property: 'version', checked: false, sort: false },
      { name: '总时长', property: 'timeCount', checked: false, sort: false },
      { name: '状态', property: 'state', checked: false, sort: false },
    ],
    pwdLength: 6,    //输入框个数 
    isFocus: false,  //聚焦 
    inputPwd: "",    //输入的内容 
    ispassword: true, //是否密文显示 true为密文， false为明文。
    //是否触发下拉刷新
    isTop: true,
    touchStartY: 0,
    touchMoveHeight: 0
  },
  onLoad: function () {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    let user = my.getStorageSync({ key: 'user' }).data;
    self.setData({
      shopId: shopId,
      user: user,
      page: 1,
      loadAll: false
    })
    self.findAllByShopID();
  },
  computeScrollViewHeight() {
    let that = this
    let query = my.createSelectorQuery().in(this)
    query.select('.mySwiper').boundingClientRect()
    query.select('.listHeader').boundingClientRect()
    query.exec(res => {
      let searchHeight = res[0].height
      let titleHeight = res[1].height
      let windowHeight = my.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - searchHeight - titleHeight - 60
      this.setData({ boxHeight: scrollHeight })
    })
  },
  findAllByShopID: function () {
    let self = this;
    self.setData({
      scrolltoupper: false,
      scrolltolower: false,
      touchMoveHeight: 0
    })
    let url = app.globalData.Host + '/equipment/findAllByShopID';
    let shopId = self.data.shopId;
    let page = self.data.page;
    let pageSize = self.data.pageSize;
    let data = {
      shopID: shopId,
      curPage: page,
      pageSize: pageSize
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        var arr = (page > 1) ? self.data.equipmentList : [];
        if (resData.length > 0) {
          for (var i = 0; i < resData.length; i++) {
            //图片字符串转数组
            resData[i].equImg = resData[i].kinds.url.split(',');
            arr.push(resData[i])
          }
          self.setData({
            page: page + 1,
            equipmentList: arr,
            scrolltoupper: true,
            scrolltolower: true,
          })
          self.mySort(0);
          if (resData.length < pageSize) {
            self.setData({
              loadAll: true
            })
          }
        } else {
          self.setData({
            equipmentList: arr,
            loadAll: true,
            scrolltoupper: true,
            scrolltolower: true,
          })
        }
      },
      function (err) {
        self.setData({
          loadAll: false,
          scrolltoupper: true,
          scrolltolower: true,
        })
      }, true
    )
  },
  goEquDetails: function (e) {
    let self = this;
    var id = e.currentTarget.dataset.id;
    var equipmentList = self.data.equipmentList;
    for (var i = 0; i < equipmentList.length; i++) {
      if (id == equipmentList[i].id) {
        my.setStorageSync({
          key: 'equipmentDetails',
          data: equipmentList[i]
        });
        my.navigateTo({
          url: '/pages/equipmentDetails/equipmentDetails?id=' + id,
        })
        return;
      }
    }
  },
  sortFun: function (e) {
    let self = this;
    var index = e.currentTarget.dataset.index;
    var sortArr = self.data.sortArr;
    for (var i = 0; i < sortArr.length; i++) {
      sortArr[i].checked = false;
    }
    sortArr[index].checked = true;
    sortArr[index].sort = !sortArr[index].sort;
    self.mySort(index)
    self.setData({
      sortArr: sortArr
    })
  },
  mySort: function (index) {
    let self = this;
    var index = index;
    var sortArr = self.data.sortArr;
    var equipmentList = self.data.equipmentList;
    var property = sortArr[index].property;
    var sortRule = sortArr[index].sort;
    //equipmentList.sort(self.compare(property, sortRule));
    self.setData({
      equipmentList: equipmentList.sort(self.compare(property, sortRule))
    })
    //console.log(equipmentList)
  },
  compare: function (property, bol) {
    return function (a, b) {
      var value1 = a[property];
      var value2 = b[property];
      if (bol) {
        return value1 - value2;
      } else {
        return value2 - value1;
      }
    }
  },
  bindscroll: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      isTop: false
    })

  },
  touchStart: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      touchStartY: e.changedTouches[0].pageY,
      isTop: true
    })
  },
  touchMove: function (e) {
    //console.log(e)
    let self = this;
    let touchStartY = self.data.touchStartY;
    let touchMoveY = e.changedTouches[0].pageY;
    self.setData({
      touchMoveHeight: touchMoveY - touchStartY
    })
  },
  touchEnd: function (e) {
    //console.log(e)
    let self = this;
    let isTop = self.data.isTop;
    let touchStartY = self.data.touchStartY;
    let touchEndY = e.changedTouches[0].pageY;
    //console.log(isTop)
    //console.log(touchStartY)
    //console.log(touchEndY)
    if (touchEndY > touchStartY && isTop) {
      self.myPullDownRefresh();
    }
  },
  myPullDownRefresh: function () {
    let self = this;
    var scrolltoupper = self.data.scrolltoupper;
    if (scrolltoupper) {
      self.onLoad();
    }
  },
  lower(e) {
    //console.log(e)
    let self = this;
    var scrolltolower = self.data.scrolltolower;
    var loadAll = self.data.loadAll;
    if (scrolltolower && !loadAll) {
      self.findAllByShopID();
    }
    //self.findAllByShopID();
  },
  scanCode: function () {
    let self = this;
    //self.payment();
    /*let uuid = new Date().getTime();
    let version = uuid;
    let type = '测试设备';
    let data = {
      uuid: uuid,
      type : type,
      version: version,
      shopID: self.data.shopId
    }
    self.addEquipment(data);*/
    my.scan({
      success(res) {
        //console.log(res)
        // var codeData = JSON.parse(res.code);
        var uuid = self.getUrlParam(res.code, 'uuid');
        var type = self.getUrlParam(res.code, 'type');
        var version = self.getUrlParam(res.code, 'version');
        my.confirm({
          title: '添加设备',
          content: type + '  ' + version,
          cancelButtonText: '取消',
          showCancel: true,
          confirmButtonText: '确定',
          confirmColor: '#3095F9',
          success(res) {
            if (res.confirm) {
              let data = {
                uuid: uuid,
                type: type,
                version: version,
                shopID: self.data.shopId
              }
              self.addEquipment(data);
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }, fail(err) {
        console.log(err)
        my.showToast({
          content: '扫描失败',
          icon: 'none',
          duration: 1500
        })
      }
    })
  },
  payment: function () {
    let self = this;
    self.setData({
      showZqmodal: true,
      inputPwd: "",
      isFocus: true,
    })
  },
  // 结算
  checkPassWord: function () {
    let self = this;
    let url = app.globalData.Host + '/rentManage/checkPassWord';
    let phone = self.data.user.phone;
    let shopID = self.data.shopId;
    let passWord = self.data.inputPwd;
    let data = {
      phone: phone,
      shopID: shopID,
      passWord: passWord
    }
    app.globalData.post(url, data,
      function (res) {
        if (res.code == '1001') {
          self.addEquipment();
        } else {
          my.confirm({
            content: res.message,
            cancelButtonText: '忘记密码',
            showCancel: false,
            confirmButtonText: '重试',
            confirmColor: '#3095F9',
            success(res) {
              if (res.confirm) {
                self.payment();
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      function (err) {
      }, true
    )
  },
  addEquipment: function (data) {
    let self = this;
    let url = app.globalData.Host + '/equipment/addEquipment';
    app.globalData.post(url, data,
      function (res) {
        //console.log(res)
        self.onLoad();
      },
      function (err) {
      }
    )
  },
  inputPwdChange: function (e) {
    var self = this;
    console.log(e.detail.value);
    var inputValue = e.detail.value;
    self.setData({
      inputPwd: inputValue,
    })
    if (inputValue.length == 6) {
      self.setData({
        showZqmodal: false,
        isFocus: false,
      })
      self.checkPassWord();
    }
  },
  isInputPwd: function () {
    var self = this;
    self.setData({
      isFocus: true,
    })
  },
  cancel: function () {
    let self = this;
    self.setData({
      showZqmodal: false,
    })
  },
  //扫码获取参数
  getUrlParam: function (url, name) {
    let arrObj = url.split("?");
    if (arrObj.length > 1) {
      let arrPara = arrObj[1].split("&");
      let arr;
      for (let i = 0; i < arrPara.length; i++) {
        arr = arrPara[i].split("=");
        if (arr != null && arr[0] == name) {
          return arr[1];
        }
      }
      return "";
    } else {
      return "";
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (app.globalData.equipmentChange){
      app.globalData.equipmentChange = false;
      this.onLoad();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})