// pages/questionDetails/questionDetails.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Host: app.globalData.Host + '/',
    questionDetails: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let questionDetails = app.globalData.questionDetails;
    self.setData({
      questionDetails: questionDetails
    })
  },
  clickImage: function (e) {
    let self = this;
    let url = e.target.dataset.url;
    url = url.map(function (item){
      return self.data.Host + item
    })
    let current = e.target.dataset.src;
    let imagesShow = self.data.imagesShow;
    my.previewImage({
      current: current,
      urls: url,
      fail: function () {
        console.log('fail')
      },
      complete: function () {
        console.info("点击图片了");
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})