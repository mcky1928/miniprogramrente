// pages/equipmentDetails/equipmentDetails.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showZqmodal: false,
    equipmentId: '',
    equipmentDetails: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options)
    let self = this;
    var equipmentDetails = my.getStorageSync({ key: 'equipmentDetails' }).data;
    let id = equipmentDetails.id;
    self.setData({
      equipmentId: id,
      equipmentDetails: equipmentDetails
    })
  },
  goAsk: function () {
    my.navigateTo({
      url: '/pages/equipmentAsk/equipmentAsk',
    })
  },
  deleteEquipment: function () {
    let self = this;
    my.confirm({
      content: '删除该设备？',
      cancelButtonText: '取消',
      showCancel: true,
      confirmButtonText: '确定',
      confirmColor: '#3095F9',
      success(res) {
        if (res.confirm) {
          self.deleteConfirm();
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  deleteConfirm: function () {
    let self = this;
    let url = app.globalData.Host + '/equipment/deleteEquipment';
    let equipmentDetails = self.data.equipmentDetails;
    let id = equipmentDetails.id;
    let data = {
      id: id
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        app.globalData.equipmentChange = true;
        my.navigateBack({
          delta: 1
        })
      },
      function (err) {
      }
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})