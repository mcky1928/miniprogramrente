// pages/questions/questions.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Host: app.globalData.Host+ '/',
    boxHeight: '',
    shopId: '',
    selectedTab: '1',
    first: true,
    QList: [],
    orderList: [],
    equList: [],
    curPage: 1,
    curPage1: 1,
    pageSize: 10,
    loadAll: false,
    loadAll1: false,
    //是否触发下拉刷新
    isTop: true,
    touchStartY: 0,
    touchMoveHeight: 0,
    scrolltoupper: false,
    scrolltolower: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    let type = self.data.selectedTab;
    self.setData({
      shopId: shopId,
      curPage: (type == '0') ? 1 : self.data.curPage,
      curPage1: (type == '1') ? 1 : self.data.curPage1,
      loadAll: (type == '0') ? false : self.data.loadAll,
      loadAll1: (type == '1') ? false : self.data.loadAll1,
    })
    self.findAllByShopIDAndType();
  },
  findAllByShopIDAndType: function (obj) {
    let self = this;
    self.setData({
      scrolltoupper: false,
      scrolltolower: false,
      touchMoveHeight: 0
    })
    let url = app.globalData.Host + '/repair/findAllByShopIDAndType';
    let shopID = self.data.shopId;
    let type = self.data.selectedTab;
    let curPage = self.data.curPage;
    let curPage1 = self.data.curPage1;
    let pageSize = self.data.pageSize;
    let data = {
      shopID: shopID,
      type: type,
      curPage: (type == '0') ? curPage : curPage1,
      pageSize: pageSize,
    };
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        var arr = [];
        if (curPage > 1 && type == '0') {
          arr = self.data.equList
        }
        if (curPage1 > 1 && type == '1') {
          arr = self.data.orderList
        }
        if (resData.length > 0) {
          for (var i = 0; i < resData.length; i++) {
            if (resData[i].url != '' && resData[i].url != null){
              resData[i].url = resData[i].url.split(',')
            }
            arr.push(resData[i])
          }
          //console.log(arr)
          self.setData({
            curPage: (type == '0') ? self.data.curPage+1 : self.data.curPage,
            curPage1: (type == '1') ? self.data.curPage1+1 : self.data.curPage1,
            orderList: (type == '1') ? arr : self.data.orderList,
            equList: (type == '0') ? arr : self.data.equList,
            QList: arr,
            scrolltoupper: true,
            scrolltolower: true,
          })
          if (resData.length < pageSize) {
            self.setData({
              loadAll: (type == '0') ? true : self.data.loadAll,
              loadAll1: (type == '1') ? true : self.data.loadAll1,
            })
          }
        } else {
          self.setData({
            loadAll: (type == '0') ? true : self.data.loadAll,
            loadAll1: (type == '1') ? true : self.data.loadAll1,
            scrolltoupper: true,
            scrolltolower: true,
          })
        }
      },
      function (err) {
        self.setData({
          loadAll: false,
          loadAll1: false,
          scrolltoupper: true,
          scrolltolower: true,
        })
      }, true
    )
  },
  bindscroll: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      isTop: false
    })

  },
  touchStart: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      touchStartY: e.changedTouches[0].pageY,
      isTop: true
    })
  },
  touchMove: function (e) {
    //console.log(e)
    let self = this;
    let touchStartY = self.data.touchStartY;
    let touchMoveY = e.changedTouches[0].pageY;
    self.setData({
      touchMoveHeight: touchMoveY - touchStartY
    })
  },
  touchEnd: function (e) {
    //console.log(e)
    let self = this;
    let isTop = self.data.isTop;
    let touchStartY = self.data.touchStartY;
    let touchEndY = e.changedTouches[0].pageY;
    //console.log(isTop)
    //console.log(touchStartY)
    //console.log(touchEndY)
    if (touchEndY > touchStartY && isTop) {
      self.myPullDownRefresh();
    }
  },
  myPullDownRefresh: function () {
    let self = this;
    var scrolltoupper = self.data.scrolltoupper;
    if (scrolltoupper) {
      self.onLoad();
    }
  },
  lower(e) {
    //console.log(e)
    let self = this;
    var scrolltolower = self.data.scrolltolower;
    var loadAll = self.data.loadAll;
    var loadAll1 = self.data.loadAll1;
    var type = self.data.selectedTab;
    if (scrolltolower && ((!loadAll && type == '0') || (!loadAll1 && type == '1'))) {
      self.findAllByShopIDAndType();
    }
    //self.findAllByShopID();
  },
  computeScrollViewHeight() {
    let that = this
    let query = my.createSelectorQuery().in(this)
    query.select('.header-tabs').boundingClientRect()
    query.exec(res => {
      let topHeight = res[0].height
      let windowHeight = my.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - topHeight - 60
      this.setData({ boxHeight: scrollHeight })
    })
  },
  QDetails: function (e) {
    let self = this;
    let index = e.currentTarget.dataset.index;
    let questionDetails = self.data.QList[index];
    app.globalData.questionDetails = questionDetails;
    my.navigateTo({
      url: '/pages/questionDetails/questionDetails',
    })
  },
  selectTab: function (e) {
    let self = this;
    let selected = e.currentTarget.dataset.select;
    let first = self.data.first;
    self.setData({
      selectedTab: selected,
      QList: selected == '0' ? self.data.equList : self.data.orderList
    })
    if (selected == '0' && first){
      self.findAllByShopIDAndType();
      self.setData({
        first: false,
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})