// pages/login/login.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userType:'2',
    showZqmodal: false,
    resData:{},
    code:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.wxLogin();
  },
  wxLogin: function () {
    let self = this;
    my.getAuthCode({
      scope: 'auth_user',
      success(res) {
        console.log(res)
        self.setData({
          code: res.authCode
        })
      },
      fail(err) {
        console.log(err)
      }
    })
  },
  getUserInfo: function (e) {
    let self = this;
    console.log(e)
    my.showLoading({
      title: '加载中'
    });
    my.getOpenUserInfo({
      success(res) {
        var userInfo = res.nickName ? res : JSON.parse(res.response).response
        console.log(userInfo)
        app.globalData.userInfo = userInfo
        let url = app.globalData.Host + '/login/aLiLogin';
        let data = {
          authCode: self.data.code,
          userName: userInfo.nickName,
          avatarUrl: userInfo.avatar,
          type: self.data.userType
        }
        app.globalData.post(url, data,
          function (res) {
            let resData = res.data;
            self.setData({
              resData: res.data
            })
            if (resData.phone) {
              my.setStorage({
                key: 'user',
                data: resData
              })
              app.globalData.userInfo = resData;
              app.globalData.getShopList(resData.id);
            } else {
              self.setData({ showZqmodal: true });
            }
          },
          function (err) {
          }
        )
      },
      fail(err) {
        console.log(err)
        my.hideLoading({});
        my.showToast({
          content: '登录失败',
          icon: 'none',
          duration: 1500
        })
      },
      fail(err) {
        console.log(err)
        my.hideLoading({});
        my.showToast({
          content: '授权失败',
          icon: 'none',
          duration: 1500
        })
      }
    })
  },
  
  onGetAuthorize(res1) {
    console.log(res1)
    my.getPhoneNumber({
        success: (res) => {
          console.log(res)
          let encryptedData = res.response
          let self = this;
          let url = app.globalData.Host + '/login/aLiBindPhone';
          let data = {
            response: encryptedData,
            userId: self.data.resData.id,
            type: self.data.userType
          }
          app.globalData.post(url, data,
            function (res) {
              let resData = res.data;
              self.setData({
                resData: resData
              })
              my.setStorage({
                key: 'user',
                data: resData
              })
              app.globalData.userInfo = resData;
              app.globalData.getShopList(resData.id);
            },
            function (err) {
            }
          )
        },
        fail: (res) => {
            console.log(res)
            console.log('getPhoneNumber_fail')
        },
    });
  },
  goPhoneLogin: function () {
    my.navigateTo({
      url: '/pages/phoneLogin/phoneLogin',
    })
  },
  hideZqmodal: function () {
    let self = this;
    self.setData({ showZqmodal: false });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})