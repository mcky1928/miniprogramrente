// pages/withdrawCash/withdrawCash.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopId: '',
    phone: '',
    account: {
      total: ''
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let user = my.getStorageSync({ key: 'user' }).data;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    let phone = user.phone;
    self.setData({
      shopId: shopId,
      phone: phone
    })
  },
  
  withdrawCashCount: function () {
    let self = this;
    let url = app.globalData.Host + '/withdraw/withdrawBalance';
    let shopId = self.data.shopId;
    let phone = self.data.phone;
    let data = {
      shopID: shopId
    }
    app.globalData.get(url, data,
      function (res) {
        //console.log(res)
        self.setData({
          account: {
            total: res
          }
        })
      },
      function (err) {
      }, true
    )
  },
  withdrawal: function (){
    let payType = '1';
    let total = this.data.account.total;
    my.navigateTo({
      url: '/pages/payPage/payPage?payType=' + payType + '&total=' + total,
    })
  },
  withdrawDetails: function () {
    my.navigateTo({
      url: '/pages/withDrawDetails/withDrawDetails',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.withdrawCashCount();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})