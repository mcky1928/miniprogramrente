// pages/chartShow/chartShow.js
import F2 from '@antv/my-f2';
import * as dealData from '../../utils/dealData.js';

const app = getApp();

let chart = null;

function drawChart(canvas, width, height) {
  chart = new F2.Chart({
    el: canvas,
    width,
    height
  });
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ec: {
      lazyLoad: true // 延迟加载
    },
    boxHeight: 0,//计算剩余高度
    startTime: '2019-04-17',//图表数据开始时间
    endTime: '2019-04-17',//图表数据结束时间
    todayDate: '',//今天的日期
    operateCountData: [],//图表数据
    basis: '0',// 0 日  1 月 2 年
    basisArr: [
      { name: '按日统计', basis: '0' },
      { name: '按月统计', basis: '1' },
      { name: '按年统计', basis: '2' }
    ],
    // chart 数据
    noChartData: true,//未取到数据
    isLoadChart: false,//是否正在加载图表
    chartData: [],//展示的图表数据
    legendData: [],//数据分类
    xAxisData: [],//x轴信息
    seriesData: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    let self = this;
    self.setData({
      startTime: dealData.getDateStr(null, -30),
      endTime: dealData.getDateStr(null, 0),
      todayDate: dealData.getDateStr(null, 0),
    })
    self.reLoadPage();
    self.computeScrollViewHeight();
  },
  reLoadPage: function () {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.setData({
      shopId: shopId,
    })
    self.operateCount();
  },
  operateCount: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/operateCount';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.post(url, data,
      function (res) {
        //console.log(res)
        var arr = res.data;
        if (arr.length > 0) {
          for (var i = 0; i < arr.length; i++) {
            arr[i].startTime = (new Date(arr[i].startTime)).format();
            arr[i].endTime = (new Date(arr[i].endTime)).format()
          }
        }
        self.setData({
          operateCountData: res.data
        })
        if (res.data.length > 0) {
          self.dealDataByDay();
        } else {
          self.setData({
            noChartData: false
          })
        }
      },
      function (err) {
        self.setData({
          noChartData: false
        })
      }, true
    )
  },
  dealDataByDay: function () {
    let self = this;
    var bT = self.data.startTime;
    var eT = self.data.endTime;
    var basis = self.data.basis;
    let arr = self.data.operateCountData;
    var seriesData = [];
    var legendData = [];
    for (var i = 0; i < arr.length; i++) {
      if (legendData.indexOf(arr[i].type) < 0) {
        legendData.push(arr[i].type)
      }
    }
    //获取数据之间所有日期
    var xAxisData = dealData.getAll(bT, eT);
    for (var i = 0; i < legendData.length; i++) {
      var type = legendData[i];
      var arrY = [];
      for (var n = 0; n < xAxisData.length; n++) {
        var obj = {
          type: type,
          value: 0,
          date: xAxisData[n]
        }
        arrY.push(obj)
      }
      for (var j = 0; j < arr.length; j++) {
        var type1 = arr[j].type;
        var endTime1 = (new Date(arr[j].endTime)).format();
        var index1 = xAxisData.indexOf(endTime1);
        if (index1 > -1) {
          var expense1 = Number(arr[j].expense);
          if (type == type1) {
            arrY[index1].value += expense1
          }
        }
      }
      var arrY = arrY.map(function (item) {
        if (basis == '0') {
          item.date = item.date.substr(5);
        }
        if (basis == '1') {
          item.date = item.date.substr(0, 7);
        }
        if (basis == '2') {
          item.date = item.date.substr(0, 4);
        }
        return item;
      });
      var dateArr = [];
      var arrZ = []
      for(var i=0;i<arrY.length;i++) {
        var date = arrY[i].date;
        if(dateArr.indexOf(date)<0){
          dateArr.push(date)
        }
      }
      for(var i=0;i<dateArr.length;i++) {
        var date = dateArr[i];
        var obj = {
          type: '',
          value: 0,
          date: date
        }
        for(var j=0;j<arrY.length;j++) {
          if(date == arrY[j].date) {
            obj.type = arrY[j].type
            obj.value += arrY[j].value
          }
        }
        arrZ.push(obj);
      }
      seriesData = seriesData.concat(arrZ);
    }
    //console.log(legendData)
    //console.log(xAxisData)
    console.log(seriesData)
    self.setData({
      xAxisData: xAxisData,
      seriesData: seriesData
    });
    self.init_echarts(seriesData);
    self.setData({
      isLoadChart: false
    })
  },
  init_echarts: function (seriesData) {
    const data = seriesData;
    chart.clear(); // 清理所有
    chart.source(data, {
      date: {
        tickCount: 8,
        range: [ 0, 1 ],
        alias: '时间'
      },
      value: {
        tickCount: 4,
        alias: '收入'
      }
    });
    chart.tooltip({
      custom: true, // 自定义 tooltip 内容框
      onChange: function(obj) {
        const legend = chart.get('legendController').legends.top[0]; // 获取 legend
        const tooltipItems = obj.items;
        const legendItems = legend.items;
        const map = {};
        legendItems.map(item => {
          map[item.name] = F2.Util.mix({}, item);
        });
        tooltipItems.map(item => {
          const { name, value } = item;
          if (map[name]) {
            map[name].value = value;
          }
        });
        legend.setItems(Object.values(map));
      },
      onHide(tooltip) {
        const legend = chart.get('legendController').legends.top[0];
        legend.setItems(chart.getLegendItems().country);
      }
    });
    chart.axis('date', {
      label(text, index, total) {
        const textCfg = {};
        textCfg.textAlign = 'center'
        return textCfg;
      }
    });
    
    chart.line().position('date*value').color('type').adjust('stack');
    chart.render();
    return chart;
  }, 
  computeScrollViewHeight() {
    let that = this
    let query = my.createSelectorQuery().in(this)
    query.select('.datePickerBox').boundingClientRect()
    query.select('.selectBasis').boundingClientRect()
    query.exec(res => {
      let searchHeight = res[0].height
      let basisHeight = res[1].height
      let windowHeight = my.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - searchHeight - basisHeight
      this.setData({ boxHeight: scrollHeight })
    })
  },
  bindStartTimeChange(e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      startTime: e.detail.value
    })
  },

  bindEndTimeChange(e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      endTime: e.detail.value
    })
  },
  changeBasis: function (e) {
    let self = this;
    let basis = e.currentTarget.dataset.basis;
    let isLoadChart = self.data.isLoadChart;
    if (isLoadChart){
      return;
    }
    self.setData({
      basis: basis,
      isLoadChart: true
    })
    self.dealDataByDay();
  },
  touchStart(e) {
    if (this.canvas) {
      this.canvas.emitEvent('touchstart', [e]);
    }
  },
  touchMove(e) {
    if (this.canvas) {
      this.canvas.emitEvent('touchmove', [e]);
    }
  },
  touchEnd(e) {
    if (this.canvas) {
      this.canvas.emitEvent('touchend', [e]);
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    my.createSelectorQuery()
      .select('#chart1')
      .boundingClientRect()
      .exec((res) => {
        // 获取分辨率
        const pixelRatio = my.getSystemInfoSync().pixelRatio;
        // 获取画布实际宽高
        const canvasWidth = res[0].width;
        const canvasHeight = res[0].height;
        // 高清解决方案
        this.setData({
          width: canvasWidth * pixelRatio,
          height: canvasHeight * pixelRatio
        });
        const myCtx = my.createCanvasContext('chart1');
        myCtx.scale(pixelRatio, pixelRatio); // �必要！按照设置的分辨率进行放大
        const canvas = new F2.Renderer(myCtx);
        this.canvas = canvas;
        //console.log(res[0].width, res[0].height);
        drawChart(canvas, res[0].width, res[0].height);
      });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})