// pages/shop/shop.js
// import * as echarts from '../../utils/ec-canvas/echarts';
import F2 from '@antv/my-f2';
import * as dealData from '../../utils/dealData.js';

const app = getApp();

let chart = null;

function drawChart(canvas, width, height) {
  console.log(canvas)
  chart = new F2.Chart({
    el: canvas,
    width,
    height
  });
}
Page({
  data: {
    boxHeight: '',
    ec: {
      lazyLoad: true // 延迟加载
    },
    shopId: '',
    userInfo:{},
    shopInfo:{},
    Host:'',
    todayIncome: 0, //今日收入
    // chart 数据
    noChartData: true,
    chartData:[],
    legendData: [],
    xAxisData: [],
    seriesData: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let userInfo = my.getStorageSync({ key: 'user' }).data;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    let shopInfo = app.globalData.shopInfo;
    self.setData({
      shopId: shopId,
      userInfo: userInfo,
      shopInfo: shopInfo,
      Host: app.globalData.Host
    })
    self.getShopDetail();
    self.selectTodayIncomeByShopID();
    // self.echartsComponnet = self.selectComponent('#mychart');
  },
  computeScrollViewHeight() {
    let that = this
    let query = my.createSelectorQuery().in(this)
    query.select('.header').boundingClientRect()
    query.select('.shop-box').boundingClientRect()
    query.exec(res => {
      let searchHeight = res[0].height
      let titleHeight = res[1].height
      console.log(searchHeight)
      console.log(titleHeight)
      let windowHeight = my.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - searchHeight - titleHeight - 100
      this.setData({ boxHeight: scrollHeight })
    })
  },
  getShopDetail: function () {
    let self = this;
    let url = app.globalData.Host + '/shop/shopDetail';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.get(url, data,
      function (res) {
        //console.log(res)
        self.setData({
          shopInfo: res.data
        })
      },
      function (err) {
      }, true
    )
  },
  selectTodayIncomeByShopID: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/selectTodayIncomeByShopID';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.get(url, data,
      function (res) {
        self.setData({
          todayIncome: res
        })
      },
      function (err) {
      }, true
    )
  },
  operateCount: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/operateCount';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.post(url, data,
      function (res) {
        //console.log(res)
        self.setData({
          chartData: res.data
        })
        if (res.data.length>0){
          self.dealLegendData();
        }else {
          self.setData({
            noChartData: false
          })
        }
      },
      function (err) {
        self.setData({
          noChartData: false
        })
      }, true
    )
  },
  dealLegendData: function (){
    var self = this;
    var arr = self.data.chartData;
    var legendData = [];
    var xAxisData = [];
    var seriesData = [];
    for (var i = 0; i < arr.length; i++) {
      arr[i].startTime = dealData.getTime(arr[i].startTime);
      arr[i].endTime = dealData.getTime(arr[i].endTime);
      if (legendData.indexOf(arr[i].type)<0){
        legendData.push(arr[i].type)
      }
    }
    arr.sort(function (a, b) {
      return a['endTime'] - b['endTime'];
    });
    //console.log(legendData)
    //console.log(arr)
    self.setData({ 
      legendData: legendData,
      chartData: arr
    });
    var bT = dealData.getDateStr(null, -15);
    var eT = dealData.getDateStr(null, 0);
    self.dealDataByXAxis(bT, eT);
  },
  dealDataByXAxis: function (bT, eT) {
    var self = this;
    var arr = self.data.chartData; 
    var legendData = self.data.legendData;
    var seriesData = [];
    //获取数据之间所有日期
    var xAxisData = dealData.getAll(bT, eT);
    for (var i = 0; i < legendData.length; i++) {
      var type = legendData[i];
      var arrY = [];
      for (var n = 0; n < xAxisData.length; n++) {
        var obj = {
          type: type,
          value: 0,
          date: xAxisData[n]
        }
        arrY.push(obj)
      }
      for (var j = 0; j < arr.length; j++) {
        var type1 = arr[j].type;
        var endTime1 = (new Date(arr[j].endTime)).format();
        var index1 = xAxisData.indexOf(endTime1);
        if (index1 > -1) {
          var expense1 = Number(arr[j].expense);
          if (type == type1) {
            arrY[index1].value += expense1
          }
        }
      }
      seriesData = seriesData.concat(arrY);
    }
    //console.log(xAxisData)
    //console.log(seriesData)
    self.setData({
      xAxisData: xAxisData,
      seriesData: seriesData
    });
    self.init_echarts();
  },
  init_echarts: function () {
    const data = this.data.seriesData;
    console.log(data)
    chart.clear(); // 清理所有
    chart.source(data, {
      date: {
        tickCount: 8,
        range: [ 0, 1 ],
        type: 'timeCat',
        mask: 'MM-DD',
        alias: '时间'
      },
      value: {
        tickCount: 4,
        alias: '收入'
      }
    });
    chart.legend('type', {
      position: 'top',
      align: 'center'
    });
    chart.tooltip({
      showItemMarker: false,
      onShow(ev) {
        const { items } = ev;
        items[0].name = items[0].title;
      }
    });
    chart.axis('date', {
      label(text, index, total) {
        const textCfg = {};
        textCfg.textAlign = 'center'
        return textCfg;
      }
    });
    
    chart.line().position('date*value').color('type').adjust('stack');
    chart.render();
    return chart;
  },
  upDateChart: function (){
    //this.dealDataByXAxis('2019-03-18', '2019-04-17');
    chart.clear(); // 清理所有
    my.navigateTo({
      url: '/pages/chartShow/chartShow',
    })
  },
  goShopInfo: function () {
    my.navigateTo({
      url: '/pages/shopInfo/shopInfo',
    })
  },
  incomeCount: function () {
    my.navigateTo({
      url: '/pages/incomeCount/incomeCount',
    })
  },
  withdrawCash: function () {
    my.navigateTo({
      url: '/pages/withdrawCash/withdrawCash',
    })
  },
  orderHistory: function () {
    my.navigateTo({
      url: '/pages/orderHistory/orderHistory',
    })
  },
  rentSet: function () {
    my.navigateTo({
      url: '/pages/rentSet/equmanagement',
    })
  },
  touchStart(e) {
    if (this.canvas) {
      this.canvas.emitEvent('touchstart', [e]);
    }
  },
  touchMove(e) {
    if (this.canvas) {
      this.canvas.emitEvent('touchmove', [e]);
    }
  },
  touchEnd(e) {
    if (this.canvas) {
      this.canvas.emitEvent('touchend', [e]);
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight();
    my.createSelectorQuery()
      .select('#chart')
      .boundingClientRect()
      .exec((res) => {
        // 获取分辨率
        const pixelRatio = my.getSystemInfoSync().pixelRatio;
        // 获取画布实际宽高
        const canvasWidth = res[0].width;
        const canvasHeight = res[0].height;
        // 高清解决方案
        this.setData({
          width: canvasWidth * pixelRatio,
          height: canvasHeight * pixelRatio
        });
        const myCtx = my.createCanvasContext('chart');
        myCtx.scale(pixelRatio, pixelRatio); // �必要！按照设置的分辨率进行放大
        const canvas = new F2.Renderer(myCtx);
        this.canvas = canvas;
        //console.log(res[0].width, res[0].height);
        drawChart(canvas, res[0].width, res[0].height);
      });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let self = this;
    self.operateCount();
    self.selectTodayIncomeByShopID();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})