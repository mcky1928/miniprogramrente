// pages/shopInfo/shopInfo.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopId:'',
    Host: '',
    shopInfo: {},
    //更新的modal
    showModal: false,
    modalTitle: '',
    updateText: '',
    // 更新地址
    isUpdate: false,
    storeAddress: '',
    centerData: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.setData({
      shopId: shopId,
      Host: app.globalData.Host
    })
    self.getShopDetail();
  },
  getShopDetail: function () {
    let self = this;
    let url = app.globalData.Host + '/shop/shopDetail';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.get(url, data,
      function (res) {
        console.log(res)
        self.setData({
          shopInfo: res.data
        })
      },
      function (err) {
      }, true
    )
  },
  goModifyPassword: function() {
    my.navigateTo({
      url: '/pages/modifyPassword/modifyPassword',
    })
  }, 
  changeUrlmen: function (e) {
    let self = this;
    let current = e.currentTarget.dataset.src;
    my.showActionSheet({
      items: ['查看大图', '更换头像'],
      //itemColor: '#3096F9',
      success: function (res) {
        //console.log(res.tapIndex)
        if (res.tapIndex == 0) {
          my.previewImage({
            current: current,
            urls: [current],
            fail: function () {
              //console.log('fail')
            },
            complete: function () {
              //console.info("点击图片了");
            },
          })
        }
        if (res.tapIndex == 1) {
          self.chooseDoorFaceImage();
        }
      },
      fail: function (res) {
        //console.log(res.errMsg)
      }
    })
  },
  changeShopName: function () {
    let self = this;
    self.setData({
      showModal: true,
      modalTitle: '更改店铺名称',
      updateText: ''
    })
  },
  changeAddress: function () {
    my.navigateTo({
      url: '/pages/shopMap/shopMap?update=1',
    })
  },
  updateAddress: function () {
    let self = this;
    let shopInfo = self.data.shopInfo;
    shopInfo.city = self.data.storeAddress;
    shopInfo.latitude = self.data.centerData.latitude;
    shopInfo.longitude = self.data.centerData.longitude;
    self.createShopOrUpdate(shopInfo);
  },
  changeLinkman: function () {
    let self = this;
    self.setData({
      showModal: true,
      modalTitle: '更改联系人',
      updateText: ''
    })
  },
  changeLinkphone: function () {
    let self = this;
    self.setData({
      showModal: true,
      modalTitle: '更改联系方式',
      updateText: ''
    })
  }, 
  changeBusiness: function () {
    let self = this;
    my.showActionSheet({
      itemList: ['合作经营', '自主经营'],
      //itemColor: '#3096F9',
      success: function (res) {
        let shopInfo = self.data.shopInfo;
        //console.log(res.tapIndex)
        if (res.tapIndex == 0) {
          shopInfo.business = '合作经营';
        }
        if (res.tapIndex == 1) {
          shopInfo.business = '自主经营';
        }
        self.createShopOrUpdate(shopInfo);
      },
      fail: function (res) {
        //console.log(res.errMsg)
      }
    })
  },
  chooseDoorFaceImage: function () {
    var _this = this;
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        //console.log(res)
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.uploadImg(res,
          function (url) {
            let shopInfo = _this.data.shopInfo;
            shopInfo.urlMen = url;
            _this.createShopOrUpdate(shopInfo);
          })
      }
    })
  },
  uploadImg: function (res, callback) {
    var tempFilePaths = res.tempFilePaths;
    //启动上传等待中...  
    my.showToast({
      content: '更新中...',
      icon: 'loading',
      mask: true,
      duration: 10000
    })
    var uploadImgCount = 0;
    for (var i = 0, h = tempFilePaths.length; i < h; i++) {
      my.uploadFile({
        url: app.globalData.Host + '/upload/upload',
        filePath: tempFilePaths[i],
        name: 'file',
        formData: {},
        header: {
          'content-type': 'multipart/form-data'
        },
        success: function (res) {
          uploadImgCount++;
          var data = JSON.parse(res.data);
          //console.log(data)
          //console.log(data.data)
          if (data.data) {
            //如果是最后一张,则隐藏等待中  
            if (uploadImgCount == tempFilePaths.length) {
              my.hideToast();
            }
            callback(data.data)
          } else {
            my.hideToast();
            my.showToast({
              content: '更新失败',
              icon: 'none',
              duration: 1500
            })
          }

        },
        fail: function (res) {
          my.hideToast();
          my.showToast({
            content: '上传图片失败',
            icon: 'none',
            duration: 1500
          })
        }
      });
    }
  },
  updateText: function (e) {
    this.setData({
      updateText: e.detail.value
    })
  },
  confirm: function () {
    let self = this;
    let modalTitle = self.data.modalTitle;
    let shopInfo = self.data.shopInfo;
    let updateText = self.data.updateText;
    if (modalTitle == '更改店铺名称'){
      shopInfo.shopName = updateText;
    }
    if (modalTitle == '更改联系人') {
      shopInfo.linkman = updateText;
    }
    if (modalTitle == '更改联系方式') {
      var reg = /^1[345789]\d{9}$/;
      if ((reg.test(updateText))) {
        shopInfo.linkphone = updateText;
      } else {
        my.showToast({
          content: '请输入正确的手机号',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    }
    self.createShopOrUpdate(shopInfo);
  },
  cancel: function() {
    let self = this;
    self.setData({
      showModal: false,
    })
  },
  createShopOrUpdate: function (shopInfo) {
    let self = this;
    let url = app.globalData.Host + '/shop/createShopOrUpdate';
    let data = shopInfo
    app.globalData.post(url, data,
      function (res) {
        self.setData({
          showModal: false,
          shopInfo: res.data
        })
      },
      function (err) {

      }
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let self = this;
    console.log(self.data.isUpdate)
    if (self.data.isUpdate) {
      self.updateAddress();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})