// pages/introduction4/introduction4.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    businessLicenceImage: '',
    idcardImage1: '',
    idcardImage2: '',
    businessLicenceImageShow: '',
    idcardImage1Show: '',
    idcardImage2Show: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    var shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (shopInfo) {
      self.setData({
        businessLicenceImage: shopInfo.urlLicense,
        idcardImage1: shopInfo.urlCardPos,
        idcardImage2: shopInfo.urlCardNag,
        businessLicenceImageShow: shopInfo.urlLicense ? app.globalData.Host + '/' + shopInfo.urlLicense : '',
        idcardImage1Show: shopInfo.urlCardPos ? app.globalData.Host + '/' + shopInfo.urlCardPos : '',
        idcardImage2Show: shopInfo.urlCardNag ? app.globalData.Host + '/' + shopInfo.urlCardNag : '',
      })
    } 
  },
  chooseBusinessLicenceImage: function () {
    var _this = this;
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.uploadImg(res,
          function (url) {
            _this.setData({
              businessLicenceImage: url,
              businessLicenceImageShow: app.globalData.Host + '/' + url
            })
          })
      }
    })
  },
  chooseIdcardImage1: function () {
    var _this = this;
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图
        _this.uploadImg(res,
          function (url) {
            _this.setData({
              idcardImage1: url,
              idcardImage1Show: app.globalData.Host + '/' + url
            })
          })
      }
    })
  },
  chooseIdcardImage2: function () {
    var _this = this;
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.uploadImg(res,
          function (url) {
            _this.setData({
              idcardImage2: url,
              idcardImage2Show: app.globalData.Host + '/' + url
            })
          })
      }
    })
  },
  clickImage: function (e) {
    let self = this;
    let current = e.target.dataset.src;
    my.previewImage({
      current: current,
      urls: [current],
      fail: function () {
        console.log('fail')
      },
      complete: function () {
        console.info("点击图片了");
      },
    })
  },
  uploadImg: function (res, callback) {
    var tempFilePaths = res.tempFilePaths;
    //启动上传等待中...  
    my.showToast({
      content: '上传中...',
      icon: 'loading',
      mask: true,
      duration: 10000
    })
    var uploadImgCount = 0;
    for (var i = 0, h = tempFilePaths.length; i < h; i++) {
      my.uploadFile({
        url: app.globalData.Host + '/upload/upload',
        filePath: tempFilePaths[i],
        name: 'file',
        formData: {},
        header: {
          'content-type': 'multipart/form-data'
        },
        success: function (res) {
          uploadImgCount++;
          var data = JSON.parse(res.data);
          //console.log(data)
          //console.log(data.data)
          if (data.data) {
            //如果是最后一张,则隐藏等待中  
            if (uploadImgCount == tempFilePaths.length) {
              my.hideToast();
            }
            callback(data.data)
          } else {
            my.hideToast();
            my.showToast({
              content: '上传图片失败',
              icon: 'none',
              duration: 1500
            })
          }
        },
        fail: function (res) {
          my.hideToast();
          my.showToast({
            content: '上传图片失败',
            icon: 'none',
            duration: 1500
          })
        }
      });
    }
  },
  deleteBusinessLicence: function () {
    this.setData({
      businessLicenceImage: '',
      businessLicenceImageShow: ''
    })
  },
  deleteIdcardImage1: function () {
    this.setData({
      idcardImage1: '',
      idcardImage1Show: ''
    })
  },
  deleteIdcardImage2: function () {
    this.setData({
      idcardImage2: '',
      idcardImage2Show: ''
    })
  },
  next: function () {
    let self = this;
    let shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (self.data.businessLicenceImage) {
      shopInfo.urlLicense = self.data.businessLicenceImage;
    } else {
      my.showToast({
        content: '请上传营业执照图片',
        icon: 'none',
        duration: 1500
      })
      return;
    }

    if (self.data.idcardImage1) {
      shopInfo.urlCardPos = self.data.idcardImage1;
    } else {
      my.showToast({
        content: '请上传身份证正面照片',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (self.data.idcardImage2) {
      shopInfo.urlCardNag = self.data.idcardImage2;
    } else {
      my.showToast({
        content: '请上传身份证背面照片',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    my.setStorage({
      key: 'shopInfo',
      data: shopInfo
    })
    my.navigateTo({
      url: '/pages/introduction5/introduction5',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})