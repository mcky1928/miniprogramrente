// pages/modifyPassword/forgetPassword.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isCountdown: true,
    count: '30',
    phone:'',
    code:'',
    type:'',
    shopId:'',
    newPwd:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options)
    let self = this;
    var user = my.getStorageSync({ key: 'user' }).data;
    self.setData({
      type: options.type,
      shopId: options.shopId,
      phone: user.phone
    })
    self.getCode();
  },
  getCode: function () {
    let self = this;
    let phone = self.data.phone;
    let url = app.globalData.Host + '/register/getCaptcha';
    let data = {
      phone: phone
    }
    self.countdownStart();
    app.globalData.get(url, data,
      function (res) {
        
      },
      function (err) {
      }, true
    )
  },
  codeChange: function (e) {
    this.setData({
      code: e.detail.value
    })
  },
  newPwdChange: function (e) {
    this.setData({
      newPwd: e.detail.value
    })
  },
  changePwd: function () {
    let self = this;
    let type = self.data.type;
    let shopId = self.data.shopId;
    let phone = self.data.phone;
    let code = self.data.code;
    let newPwd = self.data.newPwd;
    if (!code) {
      my.showToast({
        content: '请输入验证码',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (!self.data.newPwd || !(/^\d{6}$/.test(self.data.newPwd))) {
      my.showToast({
        content: '请输入6位数字密码',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    let url = app.globalData.Host + '/personalCenter/setupNewPwd';
    let data = {
      phone: phone,
      captcha: code,
      type: type,
      shopID: shopId,
      newPwd: newPwd,
    }
    self.countdownStart();
    app.globalData.post(url, data,
      function (res) {
        my.navigateBack({
          delta:2
        })
      },
      function (err) {
      }
    )
  },
  countdownStart: function () {
    let self = this;
    self.setData({
      isCountdown: true
    })
    self.data.setInter = setInterval(function () {
      if (self.data.count > 0) {
        self.setData({
          count: self.data.count - 1
        })
      } else {
        clearInterval(self.data.setInter);
        self.setData({
          isCountdown: false,
          count: 30
        })
      }
    }, 1000)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})