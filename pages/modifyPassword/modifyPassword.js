// pages/modifyPassword/modifyPassword.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectedTab: 1, //1 普通密码 2 老板密码
    shopId:'',
    oldNormal: '',
    newNormal: '',
    confirmNormal: '',
    oldBoss: '',
    newBoss: '',
    confirmBoss: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.setData({
      shopId: shopId,
    })

  },
  selectTab : function (e) {
    let self = this;
    let selected = e.currentTarget.dataset.select;
    self.setData({
      selectedTab: selected
    })
  },
  oldNormalChange: function(e) {
    let self = this;
    self.setData({
      oldNormal: e.detail.value
    })
  },
  newNormalChange: function (e) {
    let self = this;
    self.setData({
      newNormal: e.detail.value
    })
  },
  confirmNormalChange: function (e) {
    let self = this;
    self.setData({
      confirmNormal: e.detail.value
    })
  },
  oldBossChange: function (e) {
    let self = this;
    self.setData({
      oldBoss: e.detail.value
    })
  },
  newBossChange: function (e) {
    let self = this;
    self.setData({
      newBoss: e.detail.value
    })
  },
  confirmBossChange: function (e) {
    let self = this;
    self.setData({
      confirmBoss: e.detail.value
    })
  },
  amendPwd: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/amendPwd';
    let shopId = self.data.shopId;
    let type = self.data.selectedTab;
    let oldPwd;
    let newPwd;
    let oldNormal = self.data.oldNormal;
    let newNormal = self.data.newNormal;
    let confirmNormal = self.data.confirmNormal;
    let oldBoss = self.data.oldBoss;
    let newBoss = self.data.newBoss;
    let confirmBoss = self.data.confirmBoss;
    if(type == '1') {
      if (!self.data.oldNormal || !(/^\d{6}$/.test(self.data.oldNormal))) {
        my.showToast({
          content: '原密码为6位数字密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      if (!self.data.newNormal || !(/^\d{6}$/.test(self.data.newNormal))) {
        my.showToast({
          content: '请输入6位数字新密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      if (!self.data.confirmNormal) {
        my.showToast({
          content: '请确认新密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      if (self.data.confirmNormal != self.data.newNormal) {
        my.showToast({
          content: '新密码两次输入不一致',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      oldPwd = oldNormal;
      newPwd = newNormal;
    }
    if (type == '2') {
      if (!self.data.oldBoss || !(/^\d{6}$/.test(self.data.oldBoss))) {
        my.showToast({
          content: '原密码为6位数字密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      if (!self.data.newBoss || !(/^\d{6}$/.test(self.data.newBoss))) {
        my.showToast({
          content: '请输入6位数字新密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      if (!self.data.confirmBoss) {
        my.showToast({
          content: '请确认新密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      if (self.data.confirmBoss != self.data.newBoss) {
        my.showToast({
          content: '新密码两次输入不一致',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      oldPwd = oldBoss;
      newPwd = newBoss;
    }
    let data = {
      shopID: shopId,
      oldPwd: oldPwd,
      newPwd: newPwd,
      type: type
    }
    app.globalData.get(url, data,
      function (res) {
        my.navigateBack({
          delta: 1
        })
      },
      function (err) {
      }
    )
  },
  forgetPwd: function () {
    let self = this;
    let shopId = self.data.shopId;
    let type = self.data.selectedTab;
    my.navigateTo({
      url: '/pages/modifyPassword/forgetPassword?type=' + type + '&shopId=' + shopId,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})