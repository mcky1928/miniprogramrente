// pages/equipmentAsk/equipmentAsk.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showZqmodal: false,
    images: [],
    imagesShow: [],
    imgaeCountSet: 3,
    //images: ['https://images.unsplash.com/photo-1551334787-21e6bd3ab135?w=640','https://images.unsplash.com/photo-1551214012-84f95e060dee?w=640','https://images.unsplash.com/photo-1551446591-142875a901a1?w=640'],
    //imagesShow: ['https://images.unsplash.com/photo-1551334787-21e6bd3ab135?w=640', 'https://images.unsplash.com/photo-1551214012-84f95e060dee?w=640', 'https://images.unsplash.com/photo-1551446591-142875a901a1?w=640'],
    detailsText: '',
    equipmentDetails: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    var equipmentDetails = my.getStorageSync({ key: 'equipmentDetails' }).data;
    self.setData({
      equipmentDetails: equipmentDetails
    })
  },
  
  detailsTextChange: function (e) {
    //console.log('问题', e.detail.value)
    this.setData({
      detailsText: e.detail.value
    })
  },
  chooseImage: function () {
    var _this = this;
    var images = _this.data.images;
    var imagesShow = _this.data.imagesShow;
    var count = _this.data.imgaeCountSet - images.length;
    my.chooseImage({
      count: count, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.uploadImg(res,
          function (url) {
            images.push(url);
            imagesShow.push(app.globalData.Host + '/' + url);
            _this.setData({
              images: images,
              imagesShow: imagesShow
            })
            //console.log(_this.data.images)
            //console.log(_this.data.imagesShow)
          })
      }
    })
  },
  clickImage: function (e) {
    let self = this;
    let current = e.target.dataset.src;
    let imagesShow = self.data.imagesShow;
    my.previewImage({
      current: current,
      urls: imagesShow,
      fail: function () {
        console.log('fail')
      },
      complete: function () {
        console.info("点击图片了");
      },
    })
  },
  uploadImg: function (res, callback) {
    var tempFilePaths = res.tempFilePaths;
    //启动上传等待中...  
    my.showToast({
      content: '上传中...',
      icon: 'loading',
      mask: true,
      duration: 10000
    })
    var uploadImgCount = 0;
    for (var i = 0, h = tempFilePaths.length; i < h; i++) {
      console.log(tempFilePaths)
      my.uploadFile({
        url: app.globalData.Host + '/upload/upload',
        filePath: tempFilePaths[i],
        name: 'file',
        formData: {},
        header: {
          'content-type': 'multipart/form-data'
        },
        success: function (res) {
          uploadImgCount++;
          var data = JSON.parse(res.data);
          //console.log(data)
          //console.log(data.data)
          if (data.data) {
            //如果是最后一张,则隐藏等待中  
            if (uploadImgCount == tempFilePaths.length) {
              my.hideToast();
            }
            callback(data.data)
          } else {
            my.hideToast();
            my.showToast({
              content: '上传图片失败',
              icon: 'none',
              duration: 1500
            })
          }
        },
        fail: function (res) {
          my.hideToast();
          my.showToast({
            content: '上传图片失败',
            icon: 'none',
            duration: 1500
          })
        }
      });
    }
  },
  deleteImage: function (e) {
    let self = this;
    let index = e.target.dataset.index;
    let images = self.data.images;
    let imagesShow = self.data.imagesShow;
    images.splice(index, 1);
    imagesShow.splice(index, 1);
    this.setData({
      images: images,
      imagesShow: imagesShow
    })
  },
  commit: function () {
    let self = this;
    if (/^[ ]*$/.test(self.data.detailsText)){
      my.showToast({
        content: '请填写问题描述',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    let data = {
      uuid: self.data.equipmentDetails.uuid,
      type: self.data.equipmentDetails.type,
      version: self.data.equipmentDetails.version,
      shopID: self.data.equipmentDetails.shopid,
      description: self.data.detailsText,
      url: self.data.images.join(','),
    }
    self.problemFeedback(data);
  },
  problemFeedback: function (data) {
    let self = this;
    let url = app.globalData.Host + '/equipment/equipmentProblem';
    app.globalData.post(url, data,
      function (res) {
        //console.log(res)
        app.globalData.equipmentChange = true;
        self.setData({
          showZqmodal: true
        })
        var equipmentDetails = self.data.equipmentDetails;
        equipmentDetails.state = 3;
        my.setStorageSync({
          key: 'equipmentDetails', 
          data: equipmentDetails
        });
        setTimeout(function () {
          my.navigateBack({
            delta: 1
          })
        }, 1500)
      },
      function (err) {
      },true
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})