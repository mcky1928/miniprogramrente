// pages/orderDetails/orderDetails.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: '',
    returnDetails: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options)
    let self = this;
    var returnDetails = my.getStorageSync({ key: 'returnDetails' }).data;
    returnDetails.startTime = new Date(returnDetails.startTime).getTime();
    returnDetails.endTime = new Date(returnDetails.endTime).getTime();
    self.setData({
      returnDetails: returnDetails
    })
  },
  copyFun: function () {
    my.setClipboard({
      text: this.data.orderDetails.ordernum,
      success(res) {
        my.showToast({
          content: "复制成功",
          icon: 'none',
          duration: 1500
        })
        //my.hideToast(); //隐藏复制成功的弹窗提示,根据需求可选
      }
    })
  },
  callPhone: function () {
    my.makePhoneCall({
      number: this.data.orderDetails.phone,
    })
  },
  finish: function () {
    app.globalData.indexReload = true;
    app.globalData.equipmentChange = true;
    my.switchTab({
      url: '/pages/index/index'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})