// pages/tabBar/tabBar.js
const app = getApp()
Component({
    properties: {
        propName: { // 属性名
            type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
            value: 'tabBarComponent', // 属性初始值（必填）
            observer: function(newVal, oldVal) {
                // 属性被改变时执行的函数（可选）
            }
        }
    },
 
    data: {
        tabBar: [
            {
              "current": 1,
              "pagePath": "/pages/index/index",
              "text": "租赁",
              "iconPath": "/images/tab_order_d.png",
              "selectedIconPath": "/images/tab_order_s.png"
            },
            {
              "current": 0,
              "pagePath": "/pages/equipment/equipment",
              "text": "设备",
              "iconPath": "/images/tab_equ_d.png",
              "selectedIconPath": "/images/tab_equ_s.png"
            },
            {
              "current": 0,
              "pagePath": "/pages/questions/questions",
              "text": "问题",
              "iconPath": "/images/tab_faq_d.png",
              "selectedIconPath": "/images/tab_faq_s.png"
            },
            {
              "current": 0,
              "pagePath": "/pages/shop/shop",
              "text": "门店",
              "iconPath": "/images/tab_stores_d.png",
              "selectedIconPath": "/images/tab_stores_s.png"
            }
        ]
    }, // 私有数据，可用于模版渲染
    ready: function () {
      let index = app.globalData.tabBarIndex;
      //console.log(index)
      let tabBar = this.data.tabBar;
      for (var i = 0; i < tabBar.length; i++) {
        tabBar[i].current = 0;
      }
      tabBar[index].current = 1;
      this.setData({
        tabBar: tabBar
      });
    },
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {},
 
    detached: function () {},
 
    methods: {
        onTap: function (event) {
          //console.log(event)
          let that = this;
          let index = event.currentTarget.dataset.index;
          let url = event.currentTarget.dataset.url;
          app.globalData.tabBarIndex = index;
          my.redirectTo({
            url: url
          })
          /*let tabBar = that.data.tabBar;
          for(var i=0;i<tabBar.length;i++){
            tabBar[i].current = 0;
          }
          tabBar[index].current = 1;
          that.setData({
            tabBar: tabBar
          });*/
        },
    }
});