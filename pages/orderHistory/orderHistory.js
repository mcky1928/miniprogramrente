// pages/orderHistory/orderHistory.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Host: app.globalData.Host,
    boxHeight: 0,
    sortArr: [
      { name: '日期', property: 'endTime', checked: true, sort: false },
      //{ name: '订单编号', property: 'ordernum', checked: false, sort: false },
      { name: '机型', property: 'version', checked: false, sort: false },
      { name: '使用时长', property: 'timeCount', checked: false, sort: false },
      { name: '费用', property: 'expense', checked: false, sort: false },
    ],
    list:[],
    startTime: '2019-04-17',
    endTime: '2019-04-17',
    todayDate: '',
    shopId: '',
    curPage: 1,
    pageSize: 30,
    keyWord: '近7天',
    keywordArr: ['今天', '昨天', '近7天', '近30天'],
    loadAll: false,
    scrolltoupper: false,
    scrolltolower: false,
    //是否触发下拉刷新
    isTop: true,
    touchStartY: 0,
    touchMoveHeight: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    self.setData({
      startTime: self.getDateStr(null, -7),
      endTime: self.getDateStr(null, 0),
      todayDate: self.getDateStr(null, 0),
    })
    self.reLoadPage();
  },
  reLoadPage: function () {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.setData({
      shopId: shopId,
      curPage: 1,
      loadAll: false,
    })
    self.getHistoryOrders();
  },
  computeScrollViewHeight() {
    let that = this
    let query = my.createSelectorQuery().in(this)
    query.select('.datePickerBox').boundingClientRect()
    query.select('.listHeader').boundingClientRect()
    query.exec(res => {
      let searchHeight = res[0].height
      let titleHeight = res[1].height
      let windowHeight = my.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - searchHeight - titleHeight
      this.setData({ boxHeight: scrollHeight })
    })
  },
  bindStartTimeChange(e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      startTime: e.detail.value
    })
  },

  bindEndTimeChange(e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      endTime: e.detail.value
    })
  },
  getDateStr: function (today, addDayCount) {
    var date;
    if (today) {
      date = new Date(today);
    } else {
      date = new Date();
    }
    date.setDate(date.getDate() + addDayCount);//获取AddDayCount天后的日期 
    var y = date.getFullYear();
    var m = date.getMonth() + 1;//获取当前月份的日期 
    var d = date.getDate();
    if (m < 10) {
      m = '0' + m;
    };
    if (d < 10) {
      d = '0' + d;
    };
    //console.log(y + "-" + m + "-" + d)
    return y + "-" + m + "-" + d;
  },
  getHistoryOrders: function () {
    let self = this;
    self.setData({
      scrolltoupper: false,
      scrolltolower: false,
      touchMoveHeight: 0
    })
    let url = app.globalData.Host + '/personalCenter/getHistoryOrders';
    let shopId = self.data.shopId;
    let curPage = self.data.curPage;
    let pageSize = self.data.pageSize;
    let keyWord = self.data.keyWord;
    let startTime = self.getDateStr(self.data.startTime, -1);
    let endTime = self.getDateStr(self.data.endTime, 1);
    let data = {
      shopID: shopId,
      keyWord: keyWord,
      startTime: startTime,
      endTime: endTime,
      curPage: curPage,
      pageSize: pageSize
    }
    app.globalData.get(url, data,
      function (res) {
        let resData = res.data;
        if (resData.length > 0) {
          var arr = [];
          if (curPage > 1) {
            arr = self.data.list;
          }
          for (var i = 0; i < resData.length; i++) {
            resData[i].startTime = new Date(resData[i].startTime).getTime();
            resData[i].endTime = new Date(resData[i].endTime).getTime();
            //图片字符串转数组
            resData[i].equImg = resData[i].kinds.url.split(',');
            arr.push(resData[i])
          }
          self.setData({
            curPage: curPage + 1,
            list: arr,
            scrolltoupper: true,
            scrolltolower: true,
          })
          self.mySort(0);
          if (resData.length < pageSize) {
            self.setData({
              loadAll: true
            })
          }
        } else {
          self.setData({
            list: [],
            loadAll: true,
            scrolltoupper: true,
            scrolltolower: true,
          })
        }
      },
      function (err) {
      }, true
    )
  },
  searchByDate: function () {
    let self = this;
    self.setData({
      keyWord: ''
    })
    self.reLoadPage();
  },
  searchByKey: function (e) {
    let self = this;
    var index = e.currentTarget.dataset.index;
    self.setData({
      keyWord: self.data.keywordArr[index]
    })
    self.reLoadPage();

    if (index == '0') {
      self.setData({
        startTime: self.getDateStr(self.data.todayDate, 0),
        endTime: self.getDateStr(self.data.todayDate, 0)
      })
    } else if (index == '1') {
      self.setData({
        startTime: self.getDateStr(self.data.todayDate, -1),
        endTime: self.getDateStr(self.data.todayDate, -1)
      })
    } else if (index == '2') {
      self.setData({
        startTime: self.getDateStr(self.data.todayDate, -7),
        endTime: self.getDateStr(self.data.todayDate, 0)
      })
    } else if (index == '3') {
      self.setData({
        startTime: self.getDateStr(self.data.todayDate, -30),
        endTime: self.getDateStr(self.data.todayDate, 0)
      })
    }else{}
  },
  goOrderDetails: function (e) {
    let self = this;
    var id = e.currentTarget.dataset.id;
    var list = self.data.list;
    for (var i = 0; i < list.length; i++) {
      if (id == list[i].id) {
        app.globalData.historyDetails = list[i];
        my.navigateTo({
          url: '/pages/historyDetails/historyDetails?id=' + id,
        })
        return;
      }
    }
  },
  sortFun: function (e) {
    let self = this;
    var index = e.currentTarget.dataset.index;
    var sortArr = self.data.sortArr;
    for (var i = 0; i < sortArr.length; i++) {
      sortArr[i].checked = false;
    }
    sortArr[index].checked = true;
    sortArr[index].sort = !sortArr[index].sort;
    self.mySort(index)
    self.setData({
      sortArr: sortArr
    })
  },
  mySort: function (index) {
    let self = this;
    var index = index;
    var sortArr = self.data.sortArr;
    var list = self.data.list;
    var property = sortArr[index].property;
    var sortRule = sortArr[index].sort;
    //list.sort(self.compare(property, sortRule));
    self.setData({
      list: list.sort(self.compare(property, sortRule))
    })
    //console.log(list)
  },
  compare: function (property, bol) {
    return function (a, b) {
      var value1 = a[property];
      var value2 = b[property];
      if (bol) {
        return value1 - value2;
      } else {
        return value2 - value1;
      }
    }
  },
  bindscroll: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      isTop: false
    })

  },
  touchStart: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      touchStartY: e.changedTouches[0].pageY,
      isTop: true
    })
  },
  touchMove: function (e) {
    //console.log(e)
    let self = this;
    let touchStartY = self.data.touchStartY;
    let touchMoveY = e.changedTouches[0].pageY;
    self.setData({
      touchMoveHeight: touchMoveY - touchStartY
    })
  },
  touchEnd: function (e) {
    //console.log(e)
    let self = this;
    let isTop = self.data.isTop;
    let touchStartY = self.data.touchStartY;
    let touchEndY = e.changedTouches[0].pageY;
    //console.log(isTop)
    //console.log(touchStartY)
    //console.log(touchEndY)
    if (touchEndY > touchStartY && isTop) {
      self.myPullDownRefresh();
    }
  },
  myPullDownRefresh: function () {
    let self = this;
    var scrolltoupper = self.data.scrolltoupper;
    if (scrolltoupper) {
      self.reLoadPage();
    }
  },
  lower(e) {
    //console.log(e)
    let self = this;
    var scrolltolower = self.data.scrolltolower;
    var loadAll = self.data.loadAll;
    if (scrolltolower && !loadAll) {
      self.findAllByShopID();
    }
    //self.findAllByShopID();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})