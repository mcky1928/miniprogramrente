// pages/introduction5/introduction5.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showZqmodal: false,
    corporationName: '',
    corporationID: '',
    normalPassword:'',
    confirmNormalPassword: '',
    bossPassword: '',
    confirmBossPassword: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    var shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (shopInfo) {
      self.setData({
        corporationName: shopInfo.legalName,
        corporationID: shopInfo.legalCard,
      })
    } 
  },
  corporationNameChange(e) {
    //console.log('法人姓名：', e.detail.value)
    this.setData({
      corporationName: e.detail.value
    })
  },
  corporationIDChange(e) {
    //console.log('法人身份证：', e.detail.value)
    this.setData({
      corporationID: e.detail.value
    })
  },
  normalPasswordChange(e) {
    //console.log('普通密码:', e.detail.value)
    this.setData({
      normalPassword: e.detail.value
    })
  }, 
  confirmNormalPasswordChange(e) {
    //console.log('确认普通密码：', e.detail.value)
    this.setData({
      confirmNormalPassword: e.detail.value
    })
  },
  bossPasswordChange(e) {
    //console.log('老板密码：', e.detail.value)
    this.setData({
      bossPassword: e.detail.value
    })
  },
  confirmBossPasswordChange(e) {
    //console.log('确认老板密码:', e.detail.value)
    this.setData({
      confirmBossPassword: e.detail.value
    })
  },
  next: function () {
    let self = this;
    let shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (self.data.corporationName) {
      shopInfo.legalName = self.data.corporationName;
    } else {
      my.showToast({
        content: '请输入法定代表人姓名',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (self.data.corporationID) {
      var reg = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
      if (reg.test(self.data.corporationID)) {
        shopInfo.legalCard = self.data.corporationID;
      } else {
      my.showToast({
        content: '请输入正确的身份证号',
        icon: 'none',
        duration: 1500
      })
      return;
    }
      shopInfo.legalCard = self.data.corporationID;
    } else {
      my.showToast({
        content: '请输入法定代表人身份证号码',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (self.data.normalPassword && /^\d{6}$/.test(self.data.normalPassword)) {
      if (self.data.confirmNormalPassword) {
        if (self.data.confirmNormalPassword == self.data.normalPassword) {
          shopInfo.generalPwd = self.data.normalPassword;
        } else {
          my.showToast({
            content: '普通密码两次输入不一致',
            icon: 'none',
            duration: 1500
          })
          return;
        }
      } else {
        my.showToast({
          content: '请确认普通密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    } else {
      my.showToast({
        content: '请输入6位数字普通密码',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (self.data.bossPassword && /^\d{6}$/.test(self.data.bossPassword)) {
      if (self.data.confirmBossPassword) {
        if (self.data.confirmBossPassword == self.data.bossPassword) {
          shopInfo.bossPwd = self.data.bossPassword;
        } else {
          my.showToast({
            content: '老板密码两次输入不一致',
            icon: 'none',
            duration: 1500
          })
          return;
        }
      } else {
        my.showToast({
          content: '请确认老板密码',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    } else {
      my.showToast({
        content: '请输入6位数字老板密码',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    my.setStorage({
      key: 'shopInfo',
      data: shopInfo
    })
    let url = app.globalData.Host + '/shop/createShopOrUpdate';
    let data = shopInfo
    app.globalData.post(url, data,
      function (res) {
        /*let resData = res.data;
        self.setData({
          resData: res.data
        })
        my.setStorage({
          key: 'shopInfo',
          data: resData
        })
        let shopId = res.data.id;
        my.setStorage({
          key: 'shopId',
          data: shopId
        })*/
        self.setData({
          showZqmodal: true
        })
        setTimeout(function () {
          my.redirectTo({
            url: '/pages/verifyWaiting/verifyWaiting',
          })
        }, 1500)
      },
      function (err) {
      }
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})