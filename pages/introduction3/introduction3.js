// pages/introduction3/introduction3.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeAddress:'',
    centerData: {},
    storeDetailsAddress:'',
    contactName:'',
    contactPhone:'',
    doorFaceImage: '',
    operatingImage: '',
    doorFaceImageShow: '',
    operatingImageShow: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    var shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (shopInfo) {
      var centerData = {
        latitude: shopInfo.latitude,
        longitude: shopInfo.longitude,
      }
      self.setData({
        storeAddress: shopInfo.city,
        storeDetailsAddress: shopInfo.address,
        centerData: centerData,
        contactName: shopInfo.linkman,
        contactPhone: shopInfo.linkphone,
        doorFaceImage: shopInfo.urlMen,
        operatingImage: shopInfo.urlJy,
        doorFaceImageShow: shopInfo.urlMen ? app.globalData.Host + '/' + shopInfo.urlMen : '',
        operatingImageShow: shopInfo.urlJy ? app.globalData.Host + '/' + shopInfo.urlJy : '',
      })
    } 
  },
  storeAddressChange(e) {
    console.log(e)
    my.navigateTo({
      url: '/pages/shopMap/shopMap',
    })
  },
  storeDetailsAddressChange(e) {
    //console.log('门牌号：', e.detail.value)
    this.setData({
      storeDetailsAddress: e.detail.value
    })
  },
  contactNameChange(e) {
    //console.log('联系人:', e.detail.value)
    this.setData({
      contactName: e.detail.value
    })
  },
  contactPhoneChange(e) {
    //console.log('联系电话：', e.detail.value)
    this.setData({
      contactPhone: e.detail.value
    })
  },
  chooseDoorFaceImage: function () {
    var _this = this;
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        console.log(res)
        /*_this.setData({
          doorFaceImageShow: res.tempFilePaths[0]
        })*/
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.uploadImg(res,
        function(url){
          _this.setData({
            doorFaceImage: url,
            doorFaceImageShow: app.globalData.Host + '/' + url
          })
        })
      }
    })
  }, 
  chooseOperatingImage: function () {
    var _this = this;
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.uploadImg(res,
          function (url) {
            _this.setData({
              operatingImage: url,
              operatingImageShow: app.globalData.Host + '/' + url
            })
          })
      }
    })
  },
  clickImage: function (e) {
    let self = this;
    let current = e.target.dataset.src;
    my.previewImage({
      current: current,
      urls: [current],
      fail: function () {
        console.log('fail')
      },
      complete: function () {
        console.info("点击图片了");
      },
    })
  },
  uploadImg: function (res,callback) {
    var tempFilePaths = res.tempFilePaths;
    //启动上传等待中...  
    my.showToast({
      content: '上传中...',
      icon: 'loading',
      mask: true,
      duration: 10000
    })
    var uploadImgCount = 0;
    for (var i = 0, h = tempFilePaths.length; i < h; i++) {
      my.uploadFile({
        url: app.globalData.Host + '/upload/upload',
        filePath: tempFilePaths[i],
        name: 'file',
        formData: {},
        header: {
          'content-type': 'multipart/form-data'
        },
        success: function (res) {
          uploadImgCount++;
          var data = JSON.parse(res.data);
          console.log(data)
          //console.log(data.data)
          if (data.data) {
            //如果是最后一张,则隐藏等待中  
            if (uploadImgCount == tempFilePaths.length) {
              my.hideToast();
            }
            callback(data.data)
          }else {
            my.hideToast();
            my.showToast({
              content: '上传图片失败',
              icon: 'none',
              duration: 1500
            })
          }
          
        },
        fail: function (res) {
          my.hideToast();
          my.showToast({
            content: '上传图片失败',
            icon: 'none',
            duration: 1500
          })
        }
      });
    }  
  },
  deleteDoorFace: function () {
    this.setData({
      doorFaceImage: '',
      doorFaceImageShow: ''
    })
  },
  deleteOperating: function () {
    this.setData({
      operatingImage: '',
      operatingImageShow: ''
    })
  },
  next: function () {
    let self = this;
    let shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (self.data.storeAddress) {
      shopInfo.city = self.data.storeAddress;
      shopInfo.address = self.data.storeDetailsAddress;
      shopInfo.latitude = self.data.centerData.latitude;
      shopInfo.longitude = self.data.centerData.longitude;
    } else {
      my.showToast({
        content: '请选择店铺地址',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (self.data.contactName) {
      shopInfo.linkman = self.data.contactName;
    } else {
      my.showToast({
        content: '请输入联系人姓名',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (self.data.contactPhone) {
      var reg = /^1[345789]\d{9}$/;
      if ((reg.test(self.data.contactPhone))) {
        shopInfo.linkphone = self.data.contactPhone;
      } else {
        my.showToast({
          content: '请输入正确的手机号',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    } else {
      my.showToast({
        content: '请输入联系人手机号',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    

    if (self.data.doorFaceImage) {
      shopInfo.urlMen = self.data.doorFaceImage;
    } else {
      my.showToast({
        content: '请上传门脸照片',
        icon: 'none',
        duration: 1500
      })
      return;
    }

    if (self.data.operatingImage) {
      shopInfo.urlJy = self.data.operatingImage;
    } else {
      my.showToast({
        content: '请上传经营环境照片',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    my.setStorage({
      key: 'shopInfo',
      data: shopInfo
    })
    my.navigateTo({
      url: '/pages/introduction4/introduction4',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})