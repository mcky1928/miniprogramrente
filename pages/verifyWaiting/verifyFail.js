// pages/verifyWaiting/verifyFail.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.getShopDetail(shopId);
  },

  getShopDetail: function (shopId) {
    let self = this;
    let url = app.globalData.Host + '/shop/shopDetail';
    let data = {
      shopID: shopId
    }
    app.globalData.get(url, data,
      function (res) {
        //console.log(res)
        my.setStorage({
          key: 'shopInfo',
          data: res.data
        })
      },
      function (err) {
      }, true
    )
  },
  goback: function () {
    my.navigateTo({
      url: '/pages/introduction1/introduction1',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})