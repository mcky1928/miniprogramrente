// pages/rentSet/rentSet.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rentInfo: {},
    equName: '推进器',
    price1: '10',
    price2: '20',
    price3: '300',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let index = options.index;
    let equList = app.globalData.equList;
    let rentInfo = equList[index];
    self.setData({
      rentInfo: rentInfo
    })
  },
  rentSetFinish: function () {
    let self = this;
    let url = app.globalData.Host + '/RentSet/updateRentSet';
    let rentInfo = self.data.rentInfo;
    if (rentInfo.startPrice < 0 || rentInfo.renew < 0 || rentInfo.deposit < 0) {
      my.showToast({
        content: '金额设置不能小与0',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    let data = {
      type: rentInfo.type,
      id: rentInfo.id,
      startPrice: rentInfo.startPrice,
      renew: rentInfo.renew,
      deposit: rentInfo.deposit,
    }
    app.globalData.get(url, data,
      function (res) {
        app.globalData.equUpdate = true;
        my.navigateBack({
          delta: 1
        })
      },
      function (err) {
      }
    )
  },
  inputChange(e) {
    let self = this;
    if (e.detail.value.indexOf('.')>-1){
      my.showToast({
        content: '只可设置整数',
        icon: 'none',
        duration: 1500
      })
    }
    let value = parseInt(e.detail.value);
    let property = e.currentTarget.dataset.property;
    let rentInfo = self.data.rentInfo;
    rentInfo[property] = value;
    self.setData({
      rentInfo: rentInfo
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})