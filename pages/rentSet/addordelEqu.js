// pages/rentSet/addordelEqu.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopId: '',
    type: '',
    list: [],
    equList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    let equList = app.globalData.equList;
    self.setData({
      shopId: shopId,
      equList: equList
    })
    self.selectAllKinds();
  },
  selectAllKinds: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/selectAllKinds';
    let list = self.data.list;
    let equList = self.data.equList;
    let data = {}
    app.globalData.post(url, data,
      function (res) {
        let arr = res.data;
        for (var i = 0; i < arr.length;i++) {
          let type = arr[i].type;
          arr[i].added = false;
          arr[i].delId = null;
          for (var j = 0; j < equList.length; j++) {
            if (type == equList[j].type) {
              arr[i].added = true;
              arr[i].delId = equList[j].id;
              break;
            }
          }
        }
        self.setData({
          list: arr
        })
      },
      function (err) {
      }, true
    )
  },
  add: function (e) {
    let self = this;
    let index = e.currentTarget.dataset.index;
    let list = self.data.list;
    let type = list[index].type;
    my.confirm({
      content: '添加该设备？',
      cancelButtonText: '取消',
      showCancel: true,
      confirmButtonText: '确定',
      confirmColor: '#3095F9',
      success(res) {
        if (res.confirm) {
          self.setData({
            type: type
          })
          self.addEqu(index);
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  addEqu: function (index) {
    let self = this;
    let current = index;
    let url = app.globalData.Host + '/RentSet/createRentSet';
    let type = self.data.type;
    let shopId = self.data.shopId;
    let list = self.data.list;
    let data = {
      type: type,
      shopID: shopId,
      startPrice: 0,
      renew: 0,
      deposit: 0,
    }
    app.globalData.get(url, data,
      function (res) {
        app.globalData.equUpdate = true;
        list[current].added = true;
        list[current].delId = res.data.id;
        self.setData({
          list: list
        })
      },
      function (err) {
      }
    )
  },
  del: function (e) {
    let self = this;
    let index = e.currentTarget.dataset.index;
    let list = self.data.list;
    let type = list[index].type;
    my.confirm({
      content: '删除该设备？',
      cancelButtonText: '取消',
      showCancel: true,
      confirmButtonText: '确定',
      confirmColor: '#3095F9',
      success(res) {
        if (res.confirm) {
          self.setData({
            type: type
          })
          self.delEqu(index);
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  delEqu: function (index) {
    let self = this;
    let current = index;
    let url = app.globalData.Host + '/RentSet/deleteRentSet';
    let type = self.data.type;
    let shopId = self.data.shopId;
    let list = self.data.list;
    let id = list[current].delId;
    let data = {
      id: id
    }
    app.globalData.get(url, data,
      function (res) {
        app.globalData.equUpdate = true; 
        list[current].added = false;
        list[current].delId = null;
        self.setData({
          list: list
        })
      },
      function (err) {
      }
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})