// pages/rentSet/equmanagement.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopId: '',
    equList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.setData({
      shopId: shopId
    })
    self.selectRentSetListByShopID();
  },
  selectRentSetListByShopID: function () {
    let self = this;
    let url = app.globalData.Host + '/RentSet/selectRentSetListByShopID';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.get(url, data,
      function (res) {
        self.setData({
          equList: res.data
        })
        app.globalData.equList = res.data;
      },
      function (err) {
      }, true
    )
  },
  rentSet: function (e){
    let index = e.currentTarget.dataset.index;
    my.navigateTo({
      url: '/pages/rentSet/rentSet?index='+index,
    })
  },
  /** 
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (app.globalData.equUpdate) {
      this.onLoad();
      app.globalData.equUpdate = false;
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})