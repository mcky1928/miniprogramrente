// pages/incomeCount/incomeCount.js
const dealData = require('../../utils/dealData.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    boxHeight: 0,
    startTime: '2019-04-17',
    endTime: '2019-04-17',
    todayDate: '',
    operateCountData: [],
    showData:[],
    basis: '0',// 0 日  1 月 2 年
    basisArr: [
      { name: '按日统计', basis: '0' },
      { name: '按月统计', basis: '1' },
      { name: '按年统计', basis: '2' }
    ],
    sortArr: [
      { name: '时间', property: 'date', checked: true, sort: true },
      { name: '收入', property: 'value', checked: false, sort: true },
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    self.setData({
      startTime: dealData.getDateStr(null, -30),
      endTime: dealData.getDateStr(null, 0),
      todayDate: dealData.getDateStr(null, 0),
    })
    self.reLoadPage();
    self.computeScrollViewHeight();
  },
  reLoadPage: function () {
    let self = this;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    self.setData({
      shopId: shopId,
      curPage: 1,
      loadAll: false,
    })
    self.operateCount();
  },
  operateCount: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/operateCount';
    let shopId = self.data.shopId;
    let data = {
      shopID: shopId
    }
    app.globalData.post(url, data,
      function (res) {
        //console.log(res)
        var arr = res.data;
        if (arr.length>0){
          for (var i = 0; i < arr.length; i++) {
            arr[i].startTime = (new Date(arr[i].startTime)).format();
            arr[i].endTime = (new Date(arr[i].endTime)).format()
          }
        }
        self.setData({
          operateCountData: arr
        })
        self.dealDataByDay();
      },
      function (err) {
      }, true
    )
  },
  dealDataByDay: function () {
    let self = this;
    var bT = self.data.startTime;
    var eT = self.data.endTime;
    var basis = self.data.basis;
    let arr = self.data.operateCountData;
    var showData = [];
    //获取数据之间所有日期
    var arr1 = dealData.getAll(bT, eT).map(function(item){
      if (basis == '0'){
        return item;
      }
      if (basis == '1') {
        return item.substr(0, 7);
      }
      if (basis == '2') {
        return item.substr(0, 4);
      }
    });
    var xAxisData = [...new Set(arr1)];
    var arrY = [];
    for (var n = 0; n < xAxisData.length; n++) {
      var obj = {
        date: xAxisData[n],
        value: 0
      }
      arrY.push(obj)
    }
    if (arr.length>0){
      for (var j = 0; j < arr.length; j++) {
        var endTime1 = arr[j].endTime;
        var index1
        if (basis == '0') {
          index1 = xAxisData.indexOf(endTime1);
        }
        if (basis == '1') {
          index1 = xAxisData.indexOf(endTime1.substr(0, 7));
        }
        if (basis == '2') {
          index1 = xAxisData.indexOf(endTime1.substr(0, 4));
        }
        if (index1 > -1) {
          var expense1 = Number(arr[j].expense);
          arrY[index1].value += expense1
        }
      }
    }
    //console.log(xAxisData)
    //console.log(arrY)
    self.setData({
      xAxisData: xAxisData,
      showData: arrY
    });
  },
  computeScrollViewHeight() {
    let that = this
    let query = my.createSelectorQuery().in(this)
    query.select('.datePickerBox').boundingClientRect()
    query.select('.selectBasis').boundingClientRect()
    query.select('.listHeader').boundingClientRect()
    query.exec(res => {
      let searchHeight = res[0].height
      let basisHeight = res[1].height
      let titleHeight = res[2].height
      let windowHeight = my.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - searchHeight - basisHeight - titleHeight
      this.setData({ boxHeight: scrollHeight })
    })
  },
  bindStartTimeChange(e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      startTime: e.detail.value
    })
  },

  bindEndTimeChange(e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      endTime: e.detail.value
    })
  },
  changeBasis: function (e) {
    let self = this;
    let basis = e.currentTarget.dataset.basis;
    self.setData({
      basis: basis
    })
    self.dealDataByDay();
  },
  searchByDate: function (){
    let self = this;
    let startTime = self.data.startTime;
    let endTime = self.data.endTime;
    let basis = self.data.basis;
    self.dealDataByDay(startTime, endTime, basis);
  },
  sortFun: function (e) {
    let self = this;
    var index = e.currentTarget.dataset.index;
    var sortArr = self.data.sortArr;
    for (var i = 0; i < sortArr.length; i++) {
      sortArr[i].checked = false;
    }
    sortArr[index].checked = true;
    sortArr[index].sort = !sortArr[index].sort;
    self.mySort(index)
    self.setData({
      sortArr: sortArr
    })
  },
  mySort: function (index) {
    let self = this;
    var index = index;
    var sortArr = self.data.sortArr;
    var showData = self.data.showData;
    var property = sortArr[index].property;
    var sortRule = sortArr[index].sort;
    self.setData({
      showData: showData.sort(self.compare(property, sortRule))
    })
  },
  compare: function (property, bol) {
    return function (a, b) {
      var value1 = a[property];
      var value2 = b[property];
      if (property == 'date'){
        value1 = value1.replace(/-/g,'');
        value2 = b[property].replace(/-/g, '');
      }
      if (bol) {
        return value1 - value2;
      } else {
        return value2 - value1;
      }
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})