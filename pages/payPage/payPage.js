// pages/payPage/payPage.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userType: '2',
    showZqmodal: false,
    shopId: '',
    user: '',
    phone: '',
    openid: '',
    payType: '',
    payText: '',
    total: '',
    money: '',
    pwdLength: 6,    //输入框个数 
    isFocus: false,  //聚焦 
    inputPwd: "",    //输入的内容 
    ispassword: true, //是否密文显示 true为密文， false为明文
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let payType = options.payType;
    let payText = '';
    let total = options.total;
    if (payType == '0'){
      payText = '充值'
    } else if (payType == '1') {
      payText = '提现'
    }
    my.setNavigationBar({
      title: payText
    })
    let user = my.getStorageSync({ key: 'user' }).data;
    let shopId = my.getStorageSync({ key: 'shopId' }).data;
    let phone = user.phone;
    let openid = user.uuid;
    self.setData({
      payType: payType,
      payText: payText,
      total: total,
      shopId: shopId,
      user: user,
      phone: phone,
      openid: openid
    })
  },
  moneyChange: function (e) {
    //console.log('金额', e.detail.value)
    var money;
    if (/^(\d?)+(\.\d{0,2})?$/.test(e.detail.value)) { //正则验证，提现金额小数点后不能大于两位数字
      money = e.detail.value;
    } else {
      money = e.detail.value.substring(0, e.detail.value.lastIndexOf('.') + 3);
      my.showToast({
        content: '提现金额最多两位小数',
        icon: 'none',
        duration: 1500
      })
    }
    this.setData({
      money: money
    })
  },
  pay: function () {
    var self = this;
    var payType = self.data.payType;
    var total = self.data.total;
    var money = self.data.money;
    if (payType == '1' && (total-money<0)) {
      my.showToast({
        content: '超出本次可提现余额',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    let userId = self.data.user.id;
    let openid = self.data.user.uuid;
    let shopID = self.data.shopId;
    let url = app.globalData.Host + '/WeiXin/wxWithdraw';
    let data = {
      userId: userId,
      openid: openid,
      shopID: shopID,
      money: money
    }
    app.globalData.post(url, data,
      function (res) {
        console.log(res)
        my.confirm({
          title: '提现通知',
          content: '提现申请已提交，目前不支持自动提现功能，1~3个工作日会有客服人员联系您',
          showCancel: false,
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
              my.navigateBack({
                delta: 1
              })
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        //self.payment(res.data)
      },
      function (err) {
      }, true
    )
  },
  /*payOut: function (data) {
    var self = this;
    let url = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers';
    app.globalData.post(url, data,
      function (res) {
        console.log(res)
        //self.payment(res.data)
      },
      function (err) {
      }, true
    )
    my.requestPayment({
      timeStamp: data.timeStamp,
      nonceStr: data.nonceStr,
      package: data.package,
      signType: 'MD5',
      paySign: data.paySign,
      success: function (res) {
        // success
        console.log(res);
      },
      fail: function (res) {
        // fail
        console.log(res);
      },
      complete: function (res) {
        // complete
        console.log(res);
      }
    })
  },
  withDraw: function () {
    let self = this;
    let url = app.globalData.Host + '/withdraw/withdrawCashCount';
    let shopId = self.data.shopId;
    let userId = self.data.user.id;
    let money = self.data.money;
    let password = self.data.inputPwd;
    let data = {
      shopID: shopId,
      userId: userId,
      money: money,
      password: password
    }
    app.globalData.get(url, data,
      function (res) {
        console.log(res)
        self.pay();
      },
      function (err) {
      }, true
    )
  },*/
  // 结算
  checkPassWord: function () {
    let self = this;
    let url = app.globalData.Host + '/personalCenter/checkBossPwd';
    let userId = self.data.user.id;
    let shopID = self.data.shopId;
    let passWord = self.data.inputPwd;
    let data = {
      userId: userId,
      shopID: shopID,
      passWord: passWord
    }
    app.globalData.post(url, data,
      function (res) {
        if (res.code == '1001') {
          self.pay();
        } else {
          my.showModal({
            content: res.message,
            cancelText: '忘记密码',
            confirmText: '重试',
            confirmColor: '#3095F9',
            success(res) {
              if (res.confirm) {
                self.payment();
              } else if (res.cancel) {
                console.log('用户点击取消')
                self.forgetPwd();
              }
            }
          })
        }
      },
      function (err) {
      }, true
    )
  },
  inputPwdChange: function (e) {
    var self = this;
    //console.log(e.detail.value);
    var inputValue = e.detail.value;
    self.setData({
      inputPwd: inputValue,
    })
    if (inputValue.length == 6) {
      self.setData({
        showZqmodal: false,
        isFocus: false,
      })
      self.checkPassWord();
    }
  },
  isInputPwd: function () {
    var self = this;
    self.setData({
      isFocus: true,
    })
  },
  payment: function () {
    var self = this;
    var payType = self.data.payType;
    var total = self.data.total;
    var money = self.data.money;
    if (payType == '1' && (total - money < 0)) {
      my.showToast({
        content: '超出本次可提现余额',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    self.setData({
      showZqmodal: true,
      inputPwd: "",
      isFocus: true,
    })
  }, 
  cancel: function () {
    let self = this;
    self.setData({
      showZqmodal: false,
    })
  },
  forgetPwd: function () {
    let self = this;
    let shopId = self.data.shopId;
    let type = 2;
    my.navigateTo({
      url: '/pages/modifyPassword/forgetPassword?type=' + type + '&shopId=' + shopId,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      isFocus: true
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})