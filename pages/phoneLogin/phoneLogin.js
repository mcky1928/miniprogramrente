// pages/phoneLogin/phoneLogin.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userType: '2',
    showZqmodal: false,//modal显示隐藏
    phonenumber: "",//手机号
    code: "",//验证码
    isCountdown: false,//是否开始倒数
    count: 30,//倒数时间
    setInter: "",//定时器
    resData: {},
    wxcode: '',
    role: '2'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  getCode: function () {
    let self = this;
    let phonenumber = self.data.phonenumber;
    //console.log(phonenumber)
    if (!(/^1[345789]\d{9}$/.test(phonenumber))) {
      my.showToast({
        content: '手机号码有误，请重输',
        icon: 'none',
        duration: 2000
      })
      return false;
    } else {
      let url = app.globalData.Host + '/register/getCaptcha';
      let data = {
        phone: phonenumber
      }
      app.globalData.get(url,data,
        function(res) {
          my.showToast({
            content: res.message,
            icon: 'none',
            duration: 2000
          })
          self.countdownStart();
        },
        function(err) {
        }, true
      )
    }
  },
  phoneLogin : function () {
    let self = this;
    let phonenumber = this.data.phonenumber;
    let code = this.data.code;
    if(!(/^1[345789]\d{9}$/.test(phonenumber))) {
      my.showToast({
        content: '手机号码有误，请重输',
        icon: 'none',
        duration: 2000
      })
      return false;
    } else if (!code) {
      my.showToast({
        content: '请输入验证码',
        icon: 'none',
        duration: 2000
      })
      return false;
    } else {
      let url = app.globalData.Host + '/login/doLogin';
      let data = {
        phone: phonenumber,
        captcha: code,
        type: self.data.userType,
        role: self.data.role
      }
      app.globalData.post(url, data,
        function (res) {
          let resData = res.data;
          self.setData({
            resData : res.data
          })
          console.log(self.data.resData)
          if (resData.uuid){
            my.setStorage({
              key: 'user',
              data: resData
            })
            app.globalData.userInfo = resData;
            app.globalData.getShopList(resData.id);
          } else {
            self.setData({ showZqmodal: true });
          }
        },
        function (err) {
        }
      )
    }
  },
  getUserInfo: function (e) {
    let self = this;
    console.log(e)
    my.showLoading({
      title: '加载中'
    });
    my.getAuthCode({
      scope: 'auth_user',
      success(codeRes) {
        console.log(codeRes)
        self.setData({
          authCode: codeRes.authCode
        })
        my.getOpenUserInfo({
          success(res) {
            var userInfo = res.nickName ? res : JSON.parse(res.response).response
            console.log(userInfo)
            app.globalData.userInfo = userInfo
            let url = app.globalData.Host + '/login/BindALi';
            let data = {
              authCode: codeRes.authCode,
              userName: userInfo.nickName,
              avatarUrl: userInfo.avatar,
              userId: self.data.resData.id,
              type: self.data.userType
            }
            app.globalData.post(url, data,
              function (res) {
                let resData = res.data;
                if(res.code == '1001') {
                  self.setData({
                    resData: resData
                  })
                  my.setStorage({
                    key: 'user',
                    data: resData
                  })
                  app.globalData.userInfo = resData;
                  app.globalData.getShopList(resData.id);
                }
              },
              function (err) {
                my.hideLoading({});
                my.showToast({
                  content: '绑定失败',
                  icon: 'none',
                  duration: 1500
                })
              }
            )
          },
          fail(err) {
            console.log(err)
            my.hideLoading({});
            my.showToast({
              content: '授权失败',
              icon: 'none',
              duration: 1500
            })
          }
        })
      },
      fail(err) {
        console.log(err)
      }
    })
  },
  changePhonenumber: function (e) {
    this.setData({
      phonenumber: e.detail.value
    })
  },
  changeCode: function (e) {
    this.setData({
      code: e.detail.value
    })
  },
  gowxLogin: function () {
    my.navigateBack({
      delta: 1
    })
  },
  countdownStart: function () {
    let self = this;
    self.setData({
      isCountdown: true
    })
    self.data.setInter = setInterval(function () {
      if (self.data.count > 0) {
        self.setData({
          count: self.data.count - 1
        })
      } else {
        clearInterval(self.data.setInter);
        self.setData({
          isCountdown: false,
          count: 30
        })
      }
    }, 1000)
  },
  hideZqmodal: function () {
    let self = this;
    self.setData({ showZqmodal: false });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})