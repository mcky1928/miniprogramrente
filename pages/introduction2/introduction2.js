// pages/introduction2/introduction2.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeName: '',
    storeCategories: ['游泳馆'],
    cooperationMode: '合作经营',
    checkboxItems: [
      { name: '游泳馆', value: '游泳馆', checked: 'true' },
      { name: '海滩经营', value: '海滩经营' },
      { name: '设备租赁商', value: '设备租赁商' },
      { name: '俱乐部', value: '俱乐部' },
      { name: '其他', value: '其他' },
    ],
    radioboxItems: [
      { name: '合作经营', value: '合作经营', checked: 'true' },
      { name: '自主经营', value: '自主经营' },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    my.getStorage({
      key: 'user',
      success(res) {
        //console.log(res.data)
        var shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
        if (shopInfo) {
          var type = shopInfo.type.split(',');
          var checkboxItems = self.data.checkboxItems;
          for (let i = 0; i < checkboxItems.length;i++){
            let name = checkboxItems[i].value;
            if(type.indexOf(name) > -1){
              checkboxItems[i].checked = true;
            }else{
              checkboxItems[i].checked = false;
            }
          }
          var business = shopInfo.business;
          var radioboxItems = self.data.radioboxItems;
          for (let i = 0; i < radioboxItems.length; i++) {
            let name = radioboxItems[i].value;
            if (business == name) {
              radioboxItems[i].checked = true;
            } else {
              radioboxItems[i].checked = false;
            }
          }
          self.setData({
            storeName: shopInfo.shopName,
            storeCategories: type,
            cooperationMode: shopInfo.business,
            checkboxItems: checkboxItems,
            radioboxItems: radioboxItems,
          })
        } else {
          let data = {
            userId: res.data.id,
            userId: res.data.id,
            //shopID: null,
            shopName: '',
            type: '',
            business: '',
            address: '',
            latitude: '',
            longitude: '',
            linkman: '',
            linkphone: res.data.phone,
            legalName: '',
            legalCard: '',
            generalPwd: '',
            bossPwd: '',
            city: '',
            urlMen: '',
            urlJy: '',
            urlLicense: '',
            urlCardPos: '',
            urlCardNag: '',
          }
          my.setStorage({
            key: 'shopInfo',
            data: data
          })
        }
      }
    })
  },
  stroeNameChange(e) {
    //console.log('门店名称：', e.detail.value)
    this.setData({
      storeName: e.detail.value
    })
  },
  storeCategoriesChange(e) {
    //console.log('门店类别：', e.detail.value)
    this.setData({
      storeCategories: e.detail.value
    })
  },
  cooperationModeChange(e) {
    //console.log('合作方式：', e.detail.value)
    this.setData({
      cooperationMode: e.detail.value
    })
  },
  next: function () {
    let self = this;
    let shopInfo = my.getStorageSync({ key: 'shopInfo' }).data;
    if (self.data.storeName){
      shopInfo.shopName = self.data.storeName;
    }else{
      my.showToast({
        content: '店铺名称不能为空',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    shopInfo.type = self.data.storeCategories.join(',');
    shopInfo.business = self.data.cooperationMode;
    my.setStorage({
      key: 'shopInfo',
      data: shopInfo
    })
    my.navigateTo({
      url: '/pages/introduction3/introduction3',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})