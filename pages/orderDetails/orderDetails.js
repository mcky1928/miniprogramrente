// pages/orderDetails/orderDetails.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    showZqmodal: false,
    statusBarHeight: app.globalData.statusBarHeight,
    orderId: '',
    orderDetails: {},
    pwdLength: 6,    //输入框个数 
    isFocus: false,  //聚焦 
    inputPwd: "",    //输入的内容 
    ispassword: true, //是否密文显示 true为密文， false为明文。
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options)
    let self = this;
    var orderDetails = my.getStorageSync({ key: 'orderDetails' }).data;
    var user = my.getStorageSync({ key: 'user' }).data;
    console.log(my.getStorageSync({ key: 'orderDetails' }))
    console.log(orderDetails)
    let id = orderDetails.id;
    self.setData({
      user: user,
      orderId: id,
      orderDetails: orderDetails
    })
  },

  copyFun: function () {
    my.setClipboard({
      text: this.data.orderDetails.ordernum,
      success(res) {
        my.showToast({
          content: "复制成功",
          icon: 'none',
          duration: 1500
        })
        //my.hideToast(); //隐藏复制成功的弹窗提示,根据需求可选
      }
    })
  },
  callPhone: function () {
    my.makePhoneCall({
      number: this.data.orderDetails.phone,
    })
  },
  goBack: function () {
    my.navigateBack({
      delta: 1
    })
  },
  goAsk: function () {
    my.navigateTo({
      url: '/pages/orderAsk/orderAsk',
    })
  },
  payment: function () {
    let self = this;
    self.setData({
      showZqmodal: true,
      inputPwd: "",
      isFocus: true,
    })
  }, 
  checkPassWord: function () {
    let self = this;
    let url = app.globalData.Host + '/rentManage/checkPassWord';
    let userId = self.data.user.id;
    let shopID = self.data.orderDetails.shopid;
    let passWord = self.data.inputPwd;
    let data = {
      userId: userId,
      shopID: shopID,
      passWord: passWord
    }
    app.globalData.post(url, data,
      function (res) {
        if (res.code == '1001'){
          self.returnEquipment();
        }else{
          my.confirm({
            content: res.message,
            cancelButtonText: '忘记密码',
            showCancel: true,
            confirmButtonText: '重试',
            confirmColor: '#3095F9',
            success(res) {
              if (res.confirm) {
                self.payment();
              } else if (res.cancel) {
                self.forgetPwd();
                //console.log('用户点击取消')
              }
            }
          })
        }
      },
      function (err) {
      },true
    )
  },
  returnEquipment: function () {
    let self = this;
    let url = app.globalData.Host + '/rentManage/returnEquipment';
    let uuid = self.data.orderDetails.uuid;
    let userId = self.data.user.id;
    let data = {
      uuid: uuid,
      userId: userId,
      status: 0,
      data: 'close'
    }
    app.globalData.post(url, data,
      function (res) {
        if(res.code == '1001'){
          my.setStorageSync({
            key: 'returnDetails', 
            data: res.data
          });
          my.redirectTo({
            url: '/pages/returnFinish/returnFinish',
          })
        }else {

        }
      },
      function (err) {
      }
    )
  },
  inputPwdChange: function (e) {
    var self = this;
    //console.log(e.detail.value);
    var inputValue = e.detail.value;
    self.setData({
      inputPwd: inputValue,
    })
    if(inputValue.length == 6) {
      self.setData({
        showZqmodal: false,
        isFocus: false,
      })
      self.checkPassWord();
    }
  },
  isInputPwd: function () {
    var self = this;
    self.setData({
      isFocus: true,
    })
  }, 
  cancel: function () {
    let self = this;
    self.setData({
      showZqmodal: false,
    })
  },
  forgetPwd: function () {
    let self = this;
    let shopId = self.data.shopId;
    let type = 1;
    my.navigateTo({
      url: '/pages/modifyPassword/forgetPassword?type=' + type + '&shopId=' + shopId,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})