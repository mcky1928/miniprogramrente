//app.js
App({
  onLaunch: function (options) {
    // 第一次打开
    // console.log(options.query);
    // {number:1}
    // console.log(options.path);
    //打开小程序，获取本地user信息，没有跳到登录页面
    var self = this;
    my.showLoading({
      title: '加载中',
      mask: true
    });
    my.setStorage({
                key: 'user',
                data: null
              })
    var user = my.getStorageSync({ key: 'user' }).data;
    console.log(user)
    if (user) {
      self.globalData.getShopList(user.id)
    } else {
      setTimeout(function () {
        my.hideLoading({});
        my.redirectTo({
          url: '/pages/login/login',
        })
      }, 1500)
    }

    // 获取用户信息
    my.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo'] != undefined && res.authSetting['scope.userInfo'] != true) {//非初始化进入该页面,且未授权
          my.confirm({
            title: '微信授权',
            content: '需要获取您的用户信息，请确认授权，否则可能无法正常使用小程序',
            success: function (res) {
              //console.log(res)
              if (res.cancel) {
              } else if (res.confirm) {
                my.openSetting({
                  success: function (dataAu) {
                    //console.log(dataAu)
                    if (dataAu.authSetting["scope.userInfo"] == true) {
                      my.showToast({
                        content: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                    } else {
                      my.showToast({
                        content: '授权失败',
                        icon: 'success',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {//初始化进入 
        }
        else { //授权后默认加载
        }
      }
    })
    // 登录
    /*
    my.getNetworkType({
      success(res) {
        console.log(res.networkType)
        return
      }
    })
    my.login({
      success: res => {
        console.log(res)
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    */
  },
  globalData: {
    indexReload: false,//判断租赁列表是否需要刷新
    equipmentChange: false, //判断设备列表是否需要刷新
    equUpdate: false,//判断租金设置是否更新
    userInfo: null,//用户资料
    shopInfo: {}, //店铺信息
    tabBarIndex: 0, //自定义tabbar使用
    equList: [],//现已添加设备列表
    historyDetails: {}, //历史订单详情
    questionDetails:{}, //问题详情
    statusBarHeight: my.getSystemInfoSync()['statusBarHeight'],//获取导航栏高度
    Host: 'https://api.chinadive-robot.com',//服务器地址
    get: function (url, data, successCallback,errCallback,loading) {
      var loading = loading ? loading : false;
      my.showLoading({
        title: '加载中',
        mask: true
      });
      my.request({
        url: url,
        data: data,
        headers: {
          'content-type': 'application/json' // 默认值
        },
        success(res) {
          console.log(res)
          if (res.status == '200'){
            if (res.data.code == '1001') {
              if (!loading) {
                my.showToast({
                  content: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            } else {
              if (!loading) {
                my.showToast({
                  content: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            }
            setTimeout(function () {
              my.hideLoading({});
              successCallback(res.data);
            }, 1500)
          } else {
            my.showToast({
              content: '加载失败',
              icon: 'none',
              duration: 1500
            })
          }
        },
        fail(err) {
          console.log(err);
          my.hideLoading({});
          my.showToast({
            content: '请检查网络',
            icon: 'none',
            duration: 1500
          })
          errCallback(err);
        }
      })
    },
    post: function (url, data, successCallback, errCallback,loading) {
      var loading = loading ? loading : false;
      my.showLoading({
        title: '加载中',
        mask: true
      });
      my.request({
        url: url,
        data: data,
        method: 'POST',
        headers: {
          'content-type': 'application/x-www-form-urlencoded' 
        },
        success(res) {
          console.log(res)
          if(res.status == '200'){
            if (res.data.code == '1001') {
              if (!loading) {
                my.showToast({
                  content: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            } else {
              if (!loading) {
                my.showToast({
                  content: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            }
            setTimeout(function () {
              my.hideLoading({});
              successCallback(res.data);
            }, 1500)
          }else {
            my.showToast({
              content: '加载失败',
              icon: 'none',
              duration: 1500
            })
          }
        },
        fail(err) {
          my.hideLoading({});
          my.showToast({
            content: '请检查网络',
            icon: 'none',
            duration: 1500
          })
          console.log(err);
          errCallback(err);
        }
      })
    },
    // 根据手机号获取店铺列表
    // 有店铺列表继续，没有创建店铺
    getShopList: function (userId) {
      let self = this;
      let url = self.Host + '/shop/shopList';
      let data = {
        userId: userId
      }
      self.get(url, data,
        function (res) {
          //console.log(res)
          var resData = res.data;
          if (resData.length > 0) {
            var shopId = resData[0].id;
            var isChecked = resData[0].checked;
            if (isChecked == '1'){
              my.setStorage({
                key: 'shopId',
                data: shopId
              })
              self.shopInfo = resData[0];
              my.switchTab({
                url: '/pages/index/index',
              })
            } else if (isChecked == '2') {
              my.setStorage({
                key: 'shopId',
                data: shopId
              })
              self.shopInfo = resData[0];
              my.redirectTo({
                url: '/pages/verifyWaiting/verifyFail',
              })
            } else{
              my.redirectTo({
                url: '/pages/verifyWaiting/verifyWaiting',
              })
            }
          } else {
            my.hideLoading({});
            my.redirectTo({
              url: '/pages/protocol/protocol',
            })
          }
        },
        function (err) {
        }, true
      )
    },
    again_getLocation: function (self) {
      let that = this;
      // 获取位置信息
      my.getSetting({
        success: (res) => {
          //console.log(res)
          if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {//非初始化进入该页面,且未授权
            my.confirm({
              title: '授权当前位置',
              content: '需要获取您的地理位置，请确认授权，否则无法获取您所需数据',
              success: function (res) {
                //console.log(res)
                if (res.cancel) {
                  my.confirm({
                    title: '定位失败',
                    content: '未获取到您的地理位置，暂时无法为你提供服务。请检查是否已关闭定位权限，或尝试重新打开小程序',
                    cancelButtonText: '取消',
                    showCancel: false,
                    confirmButtonText: '确定',
                    confirmColor: '#3095F9',
                    success(res) {
                      if (res.confirm) {
                        my.navigateBack({
                          delta: 1
                        })
                      } else if (res.cancel) {
                        //console.log('用户点击取消')
                      }
                    }
                  })
                } else if (res.confirm) {
                  my.openSetting({
                    success: function (dataAu) {
                      //console.log(dataAu)
                      if (dataAu.authSetting["scope.userLocation"] == true) {
                        my.showToast({
                          content: '授权成功',
                          icon: 'success',
                          duration: 1000
                        })
                        //再次授权，调用getLocationt的API
                        self.getLocation();
                      } else {
                        my.confirm({
                          title: '定位失败',
                          content: '未获取到您的地理位置，暂时无法为你提供服务。请检查是否已关闭定位权限，或尝试重新打开小程序',
                          cancelButtonText: '取消',
                          showCancel: false,
                          confirmButtonText: '确定',
                          confirmColor: '#3095F9',
                          success(res) {
                            if (res.confirm) {
                              my.navigateBack({
                                delta: 1
                              })
                            } else if (res.cancel) {
                              //console.log('用户点击取消')
                            }
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          } else if (res.authSetting['scope.userLocation'] == undefined) {//初始化进入
            self.getLocation();
          }
          else { //授权后默认加载
            self.getLocation();
          }
        }
      })
    },
  }
})