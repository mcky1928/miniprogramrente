var getTime = function(time) {
  return new Date(time).getTime();
}

Date.prototype.format = function () {
  var s = '';
  var mouth = (this.getMonth() + 1) >= 10 ? (this.getMonth() + 1) : ('0' + (this.getMonth() + 1));
  var day = this.getDate() >= 10 ? this.getDate() : ('0' + this.getDate());
  s += this.getFullYear() + '-'; // 获取年份。
  s += mouth + "-"; // 获取月份。
  s += day; // 获取日。
  return (s); // 返回日期。
};

function getAll(begin, end) {
  var arr = [];
  var ab = begin.split("-");
  var ae = end.split("-");
  var db = new Date();
  db.setUTCFullYear(ab[0], ab[1] - 1, ab[2]);
  var de = new Date();
  de.setUTCFullYear(ae[0], ae[1] - 1, ae[2]);
  var unixDb = db.getTime() - 24 * 60 * 60 * 1000;
  var unixDe = de.getTime() - 24 * 60 * 60 * 1000;
  for (var k = unixDb; k <= unixDe;) {
    //console.log((new Date(parseInt(k))).format());
    k = k + 24 * 60 * 60 * 1000;
    arr.push((new Date(parseInt(k))).format());
  }
  return arr;
}
function getDateStr(today, addDayCount) {
  var date;
  if (today) {
    date = new Date(today);
  } else {
    date = new Date();
  }
  date.setDate(date.getDate() + addDayCount);//获取AddDayCount天后的日期 
  var y = date.getFullYear();
  var m = date.getMonth() + 1;//获取当前月份的日期 
  var d = date.getDate();
  if (m < 10) {
    m = '0' + m;
  };
  if (d < 10) {
    d = '0' + d;
  };
  //console.log(y + "-" + m + "-" + d)
  return y + "-" + m + "-" + d;
}
module.exports = {
  getTime: getTime,
  getAll: getAll,
  getDateStr: getDateStr
}