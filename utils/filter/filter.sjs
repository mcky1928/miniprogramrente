// todo 过滤器 module 对象文件
// 格式过滤
// 价格小数点  eg: 9 => 9.00
var numToFixed = function (num, digits = 2) {
  if (typeof num !== 'number') {
    num = Number(num)
  }
  return num.toFixed(digits)
}

// 时间格式化
function add0(m) { return m < 10 ? '0' + m : m }
var formatTime = function (timestamp) {
  //shijianchuo是整数，否则要parseInt转换
  var time = getDate(timestamp);
  var y = time.getFullYear();
  var m = time.getMonth() + 1;  
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();
  return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm) + ':' + add0(s);
}
//分钟转小时加分钟
var Hm = function (min) {
  var h = Math.floor(min/60);
  var m = min%60;
  if(h>0){
    return h + '小时' + m;
  }else {
    return m;
  }
  
}
// 格式时间 eg： 2019-12-14 23:59:59 => 2019.12.14
var sliceDateToDot = function (item) {
  if (item) {
    var reg = getRegExp('-', 'g')
    return item.slice(0, 10).replace(reg, '.')
  } else {
    return ''
  }
}

var CN = function (date, cn) {
  if(cn == '0') {
    return date.replace('-', '年').replace('-', '月') + '日';
  }
  if (cn == '1') {
    return date.replace('-', '年') + '月';
  }
  if (cn == '2') {
    return date + '年';
  }
}

var moneyFormat = function (money) {
  var reg = getRegExp('^(\d?)+(\.\d{0,2})?$', 'g')
  if (reg.test(money)) { //正则验证，提现金额小数点后不能大于两位数字
    return money;
  } else {
    return money.substring(0, money.lastIndexOf('.') + 3);
  }
}
export default {
  numToFixed: numToFixed,
  formatTime: formatTime,
  Hm: Hm,
  CN: CN,
  sliceDateToDot: sliceDateToDot,
  moneyFormat: moneyFormat
}