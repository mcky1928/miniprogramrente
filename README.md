## 租赁 商家版

### 目录结构：

	app.js
		小程序逻辑处理，公用方法
	app.json
		小程序公共配置
	app.wxss
		小程序公共样式表
	/pages
		小程序页面，每个页面由4部分组成 js, wxml, json, wxss
	/imgaes
		小程序内图片文件
	/lib
		外部样式库
	/untils
		外部引用插件，及过滤器
	project.config.json
		项目配置文件
	

### 内容说明：

首先打开商家版小程序需用户登录
1. 微信登录 - 绑定手机号 授权 pages/login/login
2. 手机号验证码登录 - 绑定当前微信 pages/phoneLogin/phoneLogin

用户登录后，需注册商家，填写商家信息，信息主要包括：
1. 店铺基本信息 pages/introduction2/introduction2
2. 店铺图片信息 pages/introduction3/introduction3
3. 营业执照 pages/introduction4/introduction4
4. 法人身份证信息 pages/introduction5/introduction5
5. 设置小程序内设备关机及提现的密码 pages/introduction5/introduction5

信息填写完成提交后需等待人工审核，pages/verifyWaiting/verifyWaiting
审核通过后，才可进入小程序，使用小程序
审核未通过，需重新填写信息，重新提交等待审核 pages/verifyFail/verifyFail


商家版小程序主要内容分以下4个模块

1. 租赁列表 pages/index/index
2. 设备列表 pages/equipment/equipment
3. 问题列表 pages/questions/questions
4. 门店页面 pages/shop/shop

模块详情：

1. 租赁列表pages/index/index

	租赁列表包括查看当前在租设备列表及租赁时长和金额 一些相关信息
	当用户归还设备时，扫描设备二维码，来查看该设备的租赁详情，并根据具体情况
	选择结算订单（pages/orderDetails/orderDetails），
	或是维修报损，以及返厂维修等操作（pages/orderAsk/orderAsk）
	
2. 设备列表pages/equipment/equipment

	租赁列表包括查看当前店铺的设备列表以及当前时间的设备状态（租用中，可租用 ，维修中）
	点击某个设备可查看设备信息，包括设备已使用的总时长及使用次数等相关信息 （pages/equipmentDetails/equipmentDetails）
	也可反馈设备出现的问题 （pages/equipmentAsk/equipmentAsk）
	
	有新设备到店铺，可使用下面的扫码添加设备，扫描设备二维码来将设备添加进来，以实现租赁
	
3. 问题列表pages/questions/questions
	
	问题列表主要是为了查看以往租赁中提交过的问题 (pages/questionDetails/questionDetails)
	和一些设备问题及详情  (pages/questionDetails/questionDetails)
	
4. 门店页面pages/shop/shop

	门店页面主要包括 店铺信息 今日收入 及最近的经营收入统计折线图
	
	内页包括如下：
	
	1) 店铺基本详细信息pages/shopInfo/shopInfo
		查看或修改店铺一些基本的信息
		修改或者找回密码
	
	2) 收入统计 pages/incomeCount/incomeCount
		可查看选择日期内的 收入列表（可选 按日， 按月， 按年 查看）
		
	3) 提取现金 pages/withdrawCash/withdrawCash
		可查看账户余额，并提现到微信记录
		可查看提现历史记录
	
	4) 订单记录 pages/orderHistory/orderHistory
		可根据日期查看日期内所有的租赁记录
		
	5)	租金设置 pages/rentSet/equmanagement
		当店铺添加一个新类型的设备，需设置该类型的设备的租金才可以租金 (pages/rentSet/rentSet)
		商家也可在这里添加或删除店铺的设备种类 (pages/rentSet/addordelEqu)

	
其他：
1. 阿里图标库：lib/css/iconfont.wxss
2. 公用过滤器文件：utils/filter/filter.wxs
3. echart图标库： utils/ec-canvas
4. 腾讯地图： utils/qqmap-wx-jssdk1.2
    对应页面：pages/shopMap/shopMap

END!